package helper.izrazi;

import helper.Check;
import helper.Type;

public class naredba_skoka extends Node {
    public naredba_skoka(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 2: {
                Node first = children.get(0);
                Node second = children.get(1);

                if ((first.name.startsWith("KR_CONTINUE") || first.name.startsWith("KR_BREAK"))
                        && second.name.startsWith("TOCKAZAREZ")) {

                    Node current = this;
                    boolean hasFor = false;
                    while (current.parent != null) {
                        if (current.parent.getName().startsWith("<naredba_petlje>")) {
                            hasFor = true;
                            break;
                        }
                        current = current.parent;
                    }

                    if (!hasFor)
                        error();

                } else if (first.name.startsWith("KR_RETURN") && second.name.startsWith("TOCKAZAREZ")) {
                    Node current = this.parent;
                    boolean hasRightFunc = false;
                    while (current != null) {
                        if (current.name.startsWith("<definicija_funkcije>") && current.children.get(0).getTip().equals(Type.VOID)) {
                            hasRightFunc = true;
                            break;
                        }
                        current = current.parent;
                    }
                    if (!hasRightFunc)
                        error();
                } else
                    error();
                break;
            }
            case 3: {
                Node izraz = children.get(1);
                if (children.get(0).name.startsWith("KR_RETURN") && izraz.name.startsWith("<izraz>")
                        && children.get(2).name.startsWith("TOCKAZAREZ")) {
                    izraz.getSvojstva();

                    Node current = this.parent;
                    boolean hasRightFunc = false;
                    while (current != null) {
                        if (current.name.startsWith("<definicija_funkcije>")) {
                            hasRightFunc = Check.canCastImplicit(izraz.getTip(), current.children.get(0).getTip());
                            break;
                        }
                        current = current.parent;
                    }
                    if (!hasRightFunc)
                        error();

                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
