package helper.izrazi;

public class lista_init_deklaratora extends Node {
    public lista_init_deklaratora(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<init_deklarator>")) {
                    child.setNtip(getNtip());
                    child.getSvojstva();
                } else
                    error();
                break;
            }
            case 3: {
                Node listaInitDeklaratora2 = children.get(0);
                Node initDeklarator = children.get(2);

                if (listaInitDeklaratora2.name.startsWith("<lista_init_deklaratora>")
                        && children.get(1).name.startsWith("ZAREZ")
                        && initDeklarator.name.startsWith("<init_deklarator>")) {
                    listaInitDeklaratora2.setNtip(getNtip());
                    listaInitDeklaratora2.getSvojstva();
                    initDeklarator.setNtip(getNtip());
                    initDeklarator.getSvojstva();
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
