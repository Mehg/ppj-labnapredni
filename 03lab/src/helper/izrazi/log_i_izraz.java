package helper.izrazi;

import helper.Check;
import helper.Type;

public class log_i_izraz extends Node {
    public log_i_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<bin_ili_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node i = children.get(0);
                Node ili = children.get(2);

                if (i.name.startsWith("<log_i_izraz>")
                        && children.get(1).name.startsWith("OP_I")
                        && ili.name.startsWith("<bin_ili_izraz>")) {

                    i.getSvojstva();
                    if (!Check.canCastImplicit(i.getTip(), Type.INT))
                        error();
                    ili.getSvojstva();
                    if (!Check.canCastImplicit(ili.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
