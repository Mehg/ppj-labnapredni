package helper.izrazi;

import java.util.ArrayList;

public class lista_argumenata extends Node {
    public lista_argumenata(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<izraz_pridruzivanja>")) {
                    child.getSvojstva();
                    setTipovi(new ArrayList<>(){{ add(child.getTip());}});
                } else
                    error();
                break;
            }
            case 3: {
                Node listaArgumenata = children.get(0);
                Node izrazPridruzivanja = children.get(2);

                if (listaArgumenata.name.startsWith("<lista_argumenata>") &&
                        children.get(1).name.startsWith("ZAREZ") &&
                        izrazPridruzivanja.name.startsWith("<izraz_pridruzivanja>")) {

                    listaArgumenata.getSvojstva();
                    izrazPridruzivanja.getSvojstva();
                    setTipovi(new ArrayList<>(listaArgumenata.getTipovi()));
                    getTipovi().add(izrazPridruzivanja.getTip());
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
