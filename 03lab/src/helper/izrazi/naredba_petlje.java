package helper.izrazi;

import helper.Check;
import helper.Type;

public class naredba_petlje extends Node {
    public naredba_petlje(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 5: {
                Node izraz = children.get(2);
                Node naredba = children.get(4);
                if (children.get(0).name.startsWith("KR_WHILE") && children.get(1).name.startsWith("L_ZAGRADA")
                        && izraz.name.startsWith("<izraz>") && children.get(3).name.startsWith("D_ZAGRADA")
                        && naredba.name.startsWith("<naredba>")) {
                    izraz.getSvojstva();
                    if (!Check.canCastImplicit(izraz.getTip(), Type.INT))
                        error();
                    naredba.getSvojstva();
                } else
                    error();
                break;
            }
            case 6: {
                Node izrazNaredba1 = children.get(2);
                Node izrazNaredba2 = children.get(3);
                Node naredba = children.get(5);

                if (children.get(0).name.startsWith("KR_FOR") && children.get(1).name.startsWith("L_ZAGRADA")
                        && izrazNaredba1.name.startsWith("<izraz_naredba>") && izrazNaredba2.name.startsWith("<izraz_naredba>")
                        && children.get(4).name.startsWith("D_ZAGRADA") && naredba.name.startsWith("<naredba>")) {
                    izrazNaredba1.getSvojstva();
                    izrazNaredba2.getSvojstva();
                    if (!Check.canCastImplicit(izrazNaredba2.getTip(), Type.INT))
                        error();
                    naredba.getSvojstva();
                } else
                    error();
                break;
            }
            case 7: {
                Node izrazNaredba1 = children.get(2);
                Node izrazNaredba2 = children.get(3);
                Node izraz = children.get(4);
                Node naredba = children.get(6);

                if (children.get(0).name.startsWith("KR_FOR") && children.get(1).name.startsWith("L_ZAGRADA")
                        && izrazNaredba1.name.startsWith("<izraz_naredba>") && izrazNaredba2.name.startsWith("<izraz_naredba>")
                        && izraz.name.startsWith("<izraz>") && children.get(5).name.startsWith("D_ZAGRADA")
                        && naredba.name.startsWith("<naredba>")) {
                    izrazNaredba1.getSvojstva();
                    izrazNaredba2.getSvojstva();
                    if (!Check.canCastImplicit(izrazNaredba2.getTip(), Type.INT))
                        error();
                    izraz.getSvojstva();
                    naredba.getSvojstva();
                } else
                    error();
                break;

            }
            default:
                error();
        }
    }
}
