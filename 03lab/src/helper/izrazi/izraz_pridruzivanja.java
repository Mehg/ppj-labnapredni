package helper.izrazi;

import helper.Check;
import helper.Type;

public class izraz_pridruzivanja extends Node {
    public izraz_pridruzivanja(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<log_ili_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node postfiks = children.get(0);
                Node izrazPridruzivanja = children.get(2);

                if (postfiks.name.startsWith("<postfiks_izraz>")
                        && children.get(1).name.startsWith("OP_PRIDRUZI")
                        && izrazPridruzivanja.name.startsWith("<izraz_pridruzivanja>")) {

                    postfiks.getSvojstva();
                    if (!postfiks.getL_izraz())
                        error();
                    izrazPridruzivanja.getSvojstva();
                    if (!Check.canCastImplicit(postfiks.getTip(), izrazPridruzivanja.getTip()))
                        error();
                    setTip(postfiks.getTip());
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
