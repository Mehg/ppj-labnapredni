package helper.izrazi;


import helper.Check;
import helper.Type;
import helper.Variable;

public class init_deklarator extends Node {
    public init_deklarator(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<izravni_deklarator>")) {
                    child.setNtip(getNtip());
                    child.getSvojstva();
                    Type childType = child.getTip();

                    if (childType.equals(Type.CONST_CHAR) || childType.equals(Type.CONST_INT)
                            || childType.equals(Type.NIZ_CONST_INT) || childType.equals(Type.NIZ_CONST_CHAR))
                        error();

                } else
                    error();
                break;
            }
            case 3: {
                Node izravniDeklarator = children.get(0);
                Node inicijalizator = children.get(2);
                if (izravniDeklarator.name.startsWith("<izravni_deklarator>")
                        && children.get(1).name.startsWith("OP_PRIDRUZI")
                        && inicijalizator.name.startsWith("<inicijalizator>")) {

                    izravniDeklarator.setNtip(getNtip());
                    izravniDeklarator.getSvojstva();
                    inicijalizator.getSvojstva();

                    Type izravniDeklaratorTip = izravniDeklarator.getTip();
                    if (izravniDeklaratorTip.equals(Type.INT) || izravniDeklaratorTip.equals(Type.CHAR)
                            || izravniDeklaratorTip.equals(Type.CONST_INT) || izravniDeklaratorTip.equals(Type.CONST_CHAR)) {
                        if (!Check.canCastImplicit(inicijalizator.getTip(), izravniDeklaratorTip)) {
                            error();
                        }
                    } else if (izravniDeklaratorTip.equals(Type.NIZ_CONST_CHAR) || izravniDeklaratorTip.equals(Type.NIZ_CONST_INT)
                            || izravniDeklaratorTip.equals(Type.NIZ_INT) || izravniDeklaratorTip.equals(Type.NIZ_CHAR)) {

                        if (inicijalizator.getBrElem() > izravniDeklarator.getBrElem())
                            error();

                        Type toCast = Check.removeNiz(izravniDeklaratorTip);
                        toCast = Check.removeConst(toCast); //nije null

                        for (Type type : inicijalizator.getTipovi()) {
                            if (!Check.canCastImplicit(type, toCast))
                                error();
                        }

//                        getScope().getVarTable().getByName(izravniDeklarator.getName()).setBrElem(izravniDeklarator.getBrElem());

                    } else {
                        error();
                    }
                    Node currentScope = getScope();
                    Variable newVariable;

                    do {
                        newVariable = currentScope
                                .getVarTable()
                                .getByName(((ListVrijednosti) izravniDeklarator.children.get(0)).getValue());
                        if (newVariable != null) {
                            break;
                        } else {
                            currentScope = currentScope.parent.getScope();
                        }
                    } while (currentScope != null && currentScope.parent != null);

                    if (newVariable == null) {
                        error();
                    }

                    if (inicijalizator.getTip().equals(Type.NIZ_INT)
                            || inicijalizator.getTip().equals(Type.NIZ_CHAR)
                            || inicijalizator.getTip().equals(Type.NIZ_CONST_INT)
                            || inicijalizator.getTip().equals(Type.NIZ_CONST_CHAR)) {
                    } else if (!inicijalizator.getTip().equals(Type.FUNCTION)
                            && !inicijalizator.getTip().equals(Type.VOID)) {

                        Node actualValue = inicijalizator;

                        while (!(actualValue instanceof ListVrijednosti)) {
                            actualValue = actualValue.children.get(0);
                        }

                        newVariable.setValue(((ListVrijednosti) actualValue).getValue());
                        newVariable.setType(getNtip());

                    } else {
                        error();
                    }
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}