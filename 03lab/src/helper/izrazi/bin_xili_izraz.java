package helper.izrazi;

import helper.Check;
import helper.Type;

public class bin_xili_izraz extends Node {
    public bin_xili_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<bin_i_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node xili = children.get(0);
                Node i = children.get(2);

                if (xili.name.startsWith("<bin_xili_izraz>")
                        && children.get(1).name.startsWith("OP_BIN_XILI")
                        && i.name.startsWith("<bin_i_izraz>")) {
                    xili.getSvojstva();
                    if (!Check.canCastImplicit(xili.getTip(), Type.INT))
                        error();
                    i.getSvojstva();
                    if (!Check.canCastImplicit(i.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
