package helper.izrazi;

public class vanjska_deklaracija extends Node {
    public vanjska_deklaracija(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<definicija_funkcije>") || child.name.startsWith("<deklaracija>")) {
                    child.getSvojstva();
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
