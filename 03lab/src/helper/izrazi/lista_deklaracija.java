package helper.izrazi;

public class lista_deklaracija extends Node {
    public lista_deklaracija(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<deklaracija>")) {
                    child.getSvojstva();
                } else
                    error();
                break;
            }
            case 2: {
                Node listaDeklaracija = children.get(0);
                Node deklaracija = children.get(1);

                if (listaDeklaracija.name.startsWith("<lista_deklaracija>")
                        && deklaracija.name.startsWith("<deklaracija>")) {
                    listaDeklaracija.getSvojstva();
                    deklaracija.getSvojstva();
                }else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
