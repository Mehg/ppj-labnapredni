package helper.izrazi;

public class naredba extends Node {
    public naredba(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<slozena_naredba>")
                        || child.name.startsWith("<izraz_naredba>")
                        || child.name.startsWith("<naredba_grananja>")
                        || child.name.startsWith("<naredba_petlje>")
                        || child.name.startsWith("<naredba_skoka>")) {
                    child.getSvojstva();
                } else
                    error();
                break;
            }
        }
    }
}
