package helper.izrazi;

public class deklaracija extends Node {
    public deklaracija(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 3: {
                Node imeTipa = children.get(0);
                Node listaInitDeklaracija = children.get(1);
                if (imeTipa.name.startsWith("<ime_tipa>")
                        && listaInitDeklaracija.name.startsWith("<lista_init_deklaratora>")
                        && children.get(2).name.startsWith("TOCKAZAREZ")) {
                    imeTipa.getSvojstva();
                    listaInitDeklaracija.setNtip(imeTipa.getTip());
                    listaInitDeklaracija.getSvojstva();
                }
                break;
            }
            default:
                error();
        }
    }
}
