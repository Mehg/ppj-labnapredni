package helper.izrazi;

public class lista_naredbi extends Node {
    public lista_naredbi(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<naredba>")) {
                    child.getSvojstva();
                } else
                    error();
                break;
            }
            case 2: {
                Node listaNaredbi = children.get(0);
                Node naredba = children.get(1);
                if (listaNaredbi.name.startsWith("<lista_naredbi>") && naredba.name.startsWith("<naredba>")) {
                    listaNaredbi.getSvojstva();
                    naredba.getSvojstva();
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
