package helper.izrazi;

import helper.FuncTable;
import helper.VarTable;

public class prijevodna_jedinica extends Node {
    private VarTable vars;
    private FuncTable funcs;

    public prijevodna_jedinica(String name) {
        super(name);
        setScope(this);
        setRoot(this);
        vars = new VarTable();
        funcs = new FuncTable();
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<vanjska_deklaracija>")) {
                    child.getSvojstva();
                } else
                    error();
                break;
            }
            case 2: {
                Node prijevodna = children.get(0);
                Node vanjska = children.get(1);
                if (prijevodna.name.startsWith("<prijevodna_jedinica>") && vanjska.name.startsWith("<vanjska_deklaracija>")) {
                    prijevodna.getSvojstva();
                    vanjska.getSvojstva();
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }

    @Override
    public VarTable getVarTable() {
        return vars;
    }

    @Override
    public FuncTable getFuncTable() {
        return funcs;
    }
}
