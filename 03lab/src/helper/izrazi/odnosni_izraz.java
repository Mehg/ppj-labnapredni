package helper.izrazi;

import helper.Check;
import helper.Type;

public class odnosni_izraz extends Node {
    public odnosni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<aditivni_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node odnosni = children.get(0);
                Node aditivni = children.get(2);
                Node op = children.get(1);

                if (odnosni.name.startsWith("<odnosni_izraz>")
                        && (op.name.startsWith("OP_LT")
                        || op.name.startsWith("OP_GT")
                        || op.name.startsWith("OP_LTE")
                        || op.name.startsWith("OP_GTE"))
                        && aditivni.name.startsWith("<aditivni_izraz>")) {

                    odnosni.getSvojstva();
                    if (!Check.canCastImplicit(odnosni.getTip(), Type.INT))
                        error();
                    aditivni.getSvojstva();
                    if (!Check.canCastImplicit(aditivni.getTip(), Type.INT))
                        error();

                    setTip(Type.INT);
                    setL_izraz(false);

                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
