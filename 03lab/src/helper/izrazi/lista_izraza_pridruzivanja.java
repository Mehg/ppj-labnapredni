package helper.izrazi;

import java.util.ArrayList;

public class lista_izraza_pridruzivanja extends Node {
    public lista_izraza_pridruzivanja(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<izraz_pridruzivanja>")) {
                    child.getSvojstva();
                    setBrElem(1);
                    setTipovi(new ArrayList<>() {{
                        add(child.getTip());
                    }});
                } else
                    error();
                break;
            }
            case 3: {
                Node listaIzrazaPridruzivanja = children.get(0);
                Node izrazPridruzivanja = children.get(2);

                if (listaIzrazaPridruzivanja.name.startsWith("<lista_izraza_pridruzivanja>")
                        && children.get(1).name.startsWith("ZAREZ")
                        && izrazPridruzivanja.name.startsWith("<izraz_pridruzivanja>")) {
                    listaIzrazaPridruzivanja.getSvojstva();
                    izrazPridruzivanja.getSvojstva();
                    setTipovi(new ArrayList<>(listaIzrazaPridruzivanja.getTipovi()));
                    getTipovi().add(izrazPridruzivanja.getTip());
                    setBrElem(listaIzrazaPridruzivanja.getBrElem() + 1);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
