package helper.izrazi;

import helper.Check;
import helper.Type;

import java.util.ArrayList;
import java.util.Arrays;

public class inicijalizator extends Node {
    public inicijalizator(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node izrazPridruzivanja = children.get(0);
                if (izrazPridruzivanja.name.startsWith("<izraz_pridruzivanja>")) {
                    izrazPridruzivanja.getSvojstva();

                    Node current = izrazPridruzivanja;
                    while (current.children.size() == 1) {
                        current = current.children.get(0);
                    }
                    if (current.children.size() == 0
                            && current instanceof ListVrijednosti
                            && current.getName().startsWith("NIZ_ZNAKOVA")) {
                        ListVrijednosti list = (ListVrijednosti) current;
                        setBrElem(Check.countNizZnakova(list.getValue()) + 1);

                        setTipovi(new ArrayList<>());
                        for (int i = 0; i < getBrElem(); i++)
                            getTipovi().add(Type.CHAR);

                        setTip(Type.NIZ_CONST_CHAR);

                    } else {
                        setTip(izrazPridruzivanja.getTip());
                    }
                } else
                    error();
                break;
            }
            case 3: {
                Node listaIzrazaPridruzivanja = children.get(1);
                if (children.get(0).name.startsWith("L_VIT_ZAGRADA")
                        && listaIzrazaPridruzivanja.name.startsWith("<lista_izraza_pridruzivanja>")
                        && children.get(2).name.startsWith("D_VIT_ZAGRADA")) {
                    listaIzrazaPridruzivanja.getSvojstva();
                    setBrElem(listaIzrazaPridruzivanja.getBrElem());
                    setTipovi(listaIzrazaPridruzivanja.getTipovi());
                    if (listaIzrazaPridruzivanja.getTipovi().get(0).equals(Type.INT)
                            || listaIzrazaPridruzivanja.getTipovi().get(0).equals(Type.CONST_INT))
                        setTip(Type.NIZ_CONST_INT);
                    else if (listaIzrazaPridruzivanja.getTipovi().get(0).equals(Type.CHAR)
                            || listaIzrazaPridruzivanja.getTipovi().get(0).equals(Type.CONST_CHAR))
                        setTip(Type.NIZ_CONST_CHAR);
                }
                else
                    error();
                break;
            }
        }
    }
}

