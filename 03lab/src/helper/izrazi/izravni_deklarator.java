package helper.izrazi;

import helper.Check;
import helper.Function;
import helper.Type;
import helper.Variable;

import java.util.ArrayList;

public class izravni_deklarator extends Node {
    public izravni_deklarator(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("IDN")) {
                    if (getNtip().equals(Type.VOID)) {
                        error();
                    }

                    if (getScope().getVarTable().containsByName(((ListVrijednosti) child).getValue())) {
                        error();
                    }
                    getScope().getVarTable().addVar(getNtip(), ((ListVrijednosti) child).getValue(), null, true);
                    setTip(getNtip());

                } else {
                    error();
                }
                break;
            }
            case 4: {
                Node idn = children.get(0);
                Node lZagrada = children.get(1);
                Node parametri = children.get(2);
                Node dZagrada = children.get(3);

                if (!idn.getName().startsWith("IDN")) {
                    error();
                }

                if (lZagrada.getName().startsWith("L_UGL_ZAGRADA")
                        && dZagrada.getName().startsWith("D_UGL_ZAGRADA")
                        && parametri.getName().startsWith("BROJ")) {

                    if (getNtip().equals(Type.VOID)) {
                        error();
                    }

                    if (getScope().getVarTable().containsByName(((ListVrijednosti) idn).getValue())) {
                        error();
                    }

                    int value = Integer.parseInt(((ListVrijednosti) parametri).getValue());

                    if (value < 0 || value > 1024) {
                        error();
                    }

                    setBrElem(value);

                    setTip(Check.makeNiz(getNtip()));

                    getScope().getVarTable().addVar(getTip(), ((ListVrijednosti) idn).getValue(), null, value);

                } else if (lZagrada.getName().startsWith("L_ZAGRADA")
                        && dZagrada.getName().startsWith("D_ZAGRADA")
                        && parametri.getName().startsWith("KR_VOID")) {

                    Function testFunct = getScope().getFuncTable().getByData(((ListVrijednosti) idn).getValue(), getNtip(), new ArrayList<>());

                    if (testFunct != null
                            && (!testFunct.getReturnType().equals(getNtip())
                            || !testFunct.getParameterTypes().isEmpty())) {
                        error();
                    } else {
                        getScope().getFuncTable().addFunction(((ListVrijednosti) idn).getValue(), getNtip(), new ArrayList<>());
                        getRoot().getFuncTable().addFunction(((ListVrijednosti) idn).getValue(), getNtip(), new ArrayList<>());
                        setTip(Type.FUNCTION);
                        setPov(getNtip());
                        setTipovi(new ArrayList<>());
                    }
                } else if (lZagrada.getName().startsWith("L_ZAGRADA")
                        && dZagrada.getName().startsWith("D_ZAGRADA")
                        && parametri.getName().startsWith("<lista_parametara>")) {

                    parametri.getSvojstva();

                    Function testFunct = getScope().getFuncTable().getByData(((ListVrijednosti) idn).getValue(), getNtip(), parametri.getTipovi());

                    if (testFunct != null
                            && (!testFunct.getReturnType().equals(getNtip())
                            || !testFunct.getParameterTypes().equals(parametri.getTipovi()))) {
                        error();
                    } else {
                        getScope().getFuncTable().addFunction(((ListVrijednosti) idn).getValue(), getNtip(), parametri.getTipovi());
                        getRoot().getFuncTable().addFunction(((ListVrijednosti) idn).getValue(), getNtip(), new ArrayList<>());
                        setTip(Type.FUNCTION);
                        setPov(getNtip());
                        setTipovi(parametri.getTipovi());
                    }

                } else {
                    error();
                }

                break;
            }
            default: {
                error();
            }
        }
    }
}
