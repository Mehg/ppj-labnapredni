package helper.izrazi;

import helper.Check;
import helper.Type;

public class unarni_izraz extends Node {
    public unarni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<postfiks_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 2: {
                Node unarni = children.get(1);
                if ((children.get(0).name.startsWith("OP_INC")
                        || children.get(0).name.startsWith("OP_DEC"))
                        && unarni.name.startsWith("<unarni_izraz>")) {
                    unarni.getSvojstva();
                    if (!unarni.getL_izraz() || !Check.canCastImplicitTip(unarni, Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);

                } else if (children.get(0).name.startsWith("<unarni_operator>")
                        && children.get(1).name.startsWith("<cast_izraz>")) {
                    Node cast = children.get(1);
                    cast.getSvojstva();
                    if (!Check.canCastImplicitTip(cast, Type.INT))
                        error();

                    setTip(Type.INT);
                    setL_izraz(false);

                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
