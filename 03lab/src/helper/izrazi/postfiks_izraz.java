package helper.izrazi;

import helper.Check;
import helper.Type;

import java.util.List;

public class postfiks_izraz extends Node {
    public postfiks_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<primarni_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                    setTipovi(child.getTipovi());
                    setPov(child.getPov());
                } else
                    error();
                break;
            }
            case 2: {
                Node child = children.get(0);

                if (!child.name.startsWith("<postfiks_izraz>")) {
                    error();
                }

                Node secondChild = children.get(1);
                if (secondChild.name.startsWith("OP_INC") || secondChild.name.startsWith("OP_DEC")) {
                    child.getSvojstva();

                    if (!child.getL_izraz() || !Check.canCastImplicitTip(child, Type.INT))
                        error();

                    setTip(Type.INT);
                    setL_izraz(false);
                }
                break;
            }
            case 3: {
                Node child = children.get(0);

                if (child.name.startsWith("<postfiks_izraz>") &&
                        children.get(1).name.startsWith("L_ZAGRADA")
                        && children.get(2).name.startsWith("D_ZAGRADA")) {
                    child.getSvojstva();

                    if (child.getTip() != Type.FUNCTION || !child.getTipovi().isEmpty())
                        error();

                    setTip(child.getPov());

                    setL_izraz(false);
                } else {
                    error();
                }
                break;
            }
            case 4: {
                Node postfix = children.get(0);
                Node izraz = children.get(2);

                if (postfix.name.startsWith("<postfiks_izraz>") &&
                        children.get(1).name.startsWith("L_UGL_ZAGRADA")
                        && izraz.name.startsWith("<izraz>")
                        && children.get(3).name.startsWith("D_UGL_ZAGRADA")) {

                    postfix.getSvojstva();

                    if (postfix.getTip() != Type.NIZ_INT &&
                            postfix.getTip() != Type.NIZ_CONST_INT &&
                            postfix.getTip() != Type.NIZ_CHAR &&
                            postfix.getTip() != Type.NIZ_CONST_CHAR)
                        error();
                    izraz.getSvojstva();
                    if (!Check.canCastImplicitTip(izraz, Type.INT))
                        error();

                    Type withoutNiz = Check.removeNiz(postfix.getTip());

                    if (withoutNiz == null)
                        error();

                    setTip(withoutNiz);
                    setL_izraz(!withoutNiz.equals(Type.CONST_CHAR) && !withoutNiz.equals(Type.CONST_INT));

                } else if (postfix.name.startsWith("<postfiks_izraz>") &&
                        children.get(1).name.startsWith("L_ZAGRADA")
                        && izraz.name.startsWith("<lista_argumenata>")
                        && children.get(3).name.startsWith("D_ZAGRADA")) {

                    //izraz here acts as lista_argumenata
                    postfix.getSvojstva();
                    izraz.getSvojstva();

                    List<Type> postfixTipovi = postfix.getTipovi();
                    List<Type> izrazTipovi = izraz.getTipovi();

                    if (postfixTipovi.size() != izrazTipovi.size())
                        error();

                    for (int i = 0; i < postfixTipovi.size(); i++) {
                        if (!Check.canCastImplicit(izrazTipovi.get(i), postfixTipovi.get(i)))
                            error();
                    }

                    if (postfix.getTip() != Type.FUNCTION)
                        error();

                    setTip(postfix.getPov());
                    setL_izraz(false);

                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
