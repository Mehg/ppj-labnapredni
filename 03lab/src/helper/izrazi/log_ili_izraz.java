package helper.izrazi;

import helper.Check;
import helper.Type;

public class log_ili_izraz extends Node {
    public log_ili_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<log_i_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node logILI = children.get(0);
                Node logI = children.get(2);

                if (logILI.name.startsWith("<log_ili_izraz>")
                        && children.get(1).name.startsWith("OP_ILI")
                        && logI.name.startsWith("<log_i_izraz>")) {

                    logILI.getSvojstva();
                    if (!Check.canCastImplicit(logILI.getTip(), Type.INT))
                        error();
                    logI.getSvojstva();
                    if (!Check.canCastImplicit(logI.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
