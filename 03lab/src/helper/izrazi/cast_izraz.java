package helper.izrazi;

import helper.Check;

public class cast_izraz extends Node {
    public cast_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<unarni_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 4: {
                Node imeTipa = children.get(1);
                Node castIzraz = children.get(3);

                if (children.get(0).name.startsWith("L_ZAGRADA")
                        && imeTipa.name.startsWith("<ime_tipa>")
                        && children.get(2).name.startsWith("D_ZAGRADA")
                        && castIzraz.name.startsWith("<cast_izraz>")) {

                    imeTipa.getSvojstva();
                    castIzraz.getSvojstva();
                    if (!Check.canCastExplicit(castIzraz.getTip(), imeTipa.getTip())) {
                        error();
                    }
                    setTip(imeTipa.getTip());
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
