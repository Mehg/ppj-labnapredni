package helper.izrazi;

import helper.*;

public class primarni_izraz extends Node {

    public primarni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("IDN")) {
                    Node current = getScope();
                    boolean found = false;

                    do {
                        Function func = current.getFuncTable().getByName(((ListVrijednosti) child).getValue());
                        Variable var = current.getVarTable().getByName(((ListVrijednosti) child).getValue());

                        if (func != null) {
                            setL_izraz(false);
                            setTip(Type.FUNCTION);
                            setPov(func.getReturnType());
                            setTipovi(func.getParameterTypes());
                            found = true;
                            break;
                        } else if (var != null) {
                            setTip(var.getType());
                            setL_izraz(var.isL_izraz());
                            found = true;
                            break;
                        }
                        if (current.parent == null)
                            break;
                        current = current.parent.getScope();
                    } while (current != null);

                    if (!found)
                        error();

                } else if (child.name.startsWith("BROJ")) {
                    setTip(Type.INT);
                    setL_izraz(false);
                    if (!Check.checkInt((ListVrijednosti) child))
                        error();
                } else if (child.name.startsWith("ZNAK")) {
                    setTip(Type.CHAR);
                    setL_izraz(false);
                    if (!Check.checkZnak((ListVrijednosti) child))
                        error();
                } else if (child.name.startsWith("NIZ_ZNAKOVA")) {
                    setTip(Type.NIZ_CONST_CHAR);
                    setL_izraz(false);
                    if (!Check.checkNizZnakova((ListVrijednosti) child))
                        error();
                } else
                    error();
                break;
            }
            case 3: {
                if (children.get(0).name.startsWith("L_ZAGRADA") && children.get(1).name.startsWith("<izraz>") &&
                        children.get(2).name.startsWith("D_ZAGRADA")) {
                    children.get(1).getSvojstva();
                    setTip(children.get(1).getTip());
                    setL_izraz(children.get(1).getL_izraz());
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
