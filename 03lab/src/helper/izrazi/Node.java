package helper.izrazi;

import helper.FuncTable;
import helper.Type;
import helper.VarTable;

import java.util.ArrayList;
import java.util.List;

public class Node {
    String production;
    String name;

    Node parent;
    List<Node> children;

    private Type tip;
    private boolean l_izraz;
    private Type pov;
    private List<Type> tipovi;
    private List<String> imena;
    private String ime;
    private Type ntip;
    private int brElem;

    private Node scope; // referenca na Node koji sadrzi tablicu za taj scope (root ili slozena_naredba)

    private Node root;  // bas uvijek root, zbog potrebe provjeravanja konzistentnosti tipiziranja funkcija
    // (upute str 66., tocka 4 u void produkciji)

    //tablica njet bas ovdje

    public Node(String name) {
        this.name = name;
        children = new ArrayList<>();
        tipovi = new ArrayList<>();
        imena = new ArrayList<>();
    }

    public VarTable getVarTable() {
        return null;
    }

    public FuncTable getFuncTable() {
        return null;
    }

    public void getSvojstva() {
    }

    void error() {
        System.out.println(toString());
        System.exit(0);
    }

    public void addChild(Node node) {
        children.add(node);

        if (!(node instanceof slozena_naredba)) {
            node.setScope(scope);
        } else {
            node.setScope(node);
        }

        node.setRoot(root);

        if (node instanceof ListVrijednosti) {
            addRightSideMember(node.toString());
        } else
            addRightSideMember(node.name);
    }

    void addRightSideMember(String member) {
        if (production == null)
            production = name + " ::=";
        production = production + " " + member;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Type getTip() {
        return tip;
    }

    public void setTip(Type type) {
        this.tip = type;
    }

    public boolean getL_izraz() {
        return l_izraz;
    }

    public void setL_izraz(boolean l_izraz) {
        this.l_izraz = l_izraz;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public Node getScope() {
        return scope;
    }

    public void setScope(Node scope) {
        this.scope = scope;
    }

    public Type getPov() {
        return pov;
    }

    public void setPov(Type pov) {
        this.pov = pov;
    }

    public List<Type> getTipovi() {
        return tipovi;
    }

    public void setTipovi(List<Type> tipovi) {
        this.tipovi = tipovi;
    }

    public List<String> getImena() {
        return imena;
    }

    public void setImena(List<String> imena) {
        this.imena = imena;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public Type getNtip() {
        return ntip;
    }

    public void setNtip(Type ntip) {
        this.ntip = ntip;
    }

    public int getBrElem() {
        return brElem;
    }

    public void setBrElem(int brElem) {
        this.brElem = brElem;
    }

    @Override
    public String toString() {
        return production == null ? name : production;
    }
}
