package helper.izrazi;

import helper.Check;
import helper.Type;

public class naredba_grananja extends Node {
    public naredba_grananja(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 5: {
                Node izraz = children.get(2);
                Node naredba = children.get(4);
                if (children.get(0).name.startsWith("KR_IF") && children.get(1).name.startsWith("L_ZAGRADA")
                        && izraz.name.startsWith("<izraz>") && children.get(3).name.startsWith("D_ZAGRADA")
                        && naredba.name.startsWith("<naredba>")) {
                    izraz.getSvojstva();
                    if (!Check.canCastImplicit(izraz.getTip(), Type.INT))
                        error();
                    naredba.getSvojstva();
                } else
                    error();
                break;
            }
            case 7: {
                Node izraz = children.get(2);
                Node naredba1 = children.get(4);
                Node naredba2 = children.get(6);

                if (children.get(0).name.startsWith("KR_IF") && children.get(1).name.startsWith("L_ZAGRADA")
                        && izraz.name.startsWith("<izraz>") && children.get(3).name.startsWith("D_ZAGRADA")
                        && naredba1.name.startsWith("<naredba>") && children.get(5).name.startsWith("KR_ELSE")
                        && naredba2.name.startsWith("<naredba>")) {
                    izraz.getSvojstva();
                    if (!Check.canCastImplicit(izraz.getTip(), Type.INT))
                        error();
                    naredba1.getSvojstva();
                    naredba2.getSvojstva();
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
