package helper.izrazi;

import helper.Check;
import helper.Type;

public class bin_ili_izraz extends Node {
    public bin_ili_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<bin_xili_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node ili = children.get(0);
                Node xili = children.get(2);

                if (ili.name.startsWith("<bin_ili_izraz>")
                        && children.get(1).name.startsWith("OP_BIN_ILI")
                        && xili.name.startsWith("<bin_xili_izraz>")) {

                    ili.getSvojstva();
                    if (!Check.canCastImplicit(ili.getTip(), Type.INT))
                        error();
                    xili.getSvojstva();
                    if (!Check.canCastImplicit(xili.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
