package helper.izrazi;

import helper.Check;
import helper.Type;

public class aditivni_izraz extends Node {
    public aditivni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<multiplikativni_izraz>")) {
                    child.getSvojstva();
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node aditivni = children.get(0);
                Node multiplikativni = children.get(2);

                if (aditivni.name.startsWith("<aditivni_izraz>")
                        && (children.get(1).name.startsWith("PLUS")
                        || children.get(1).name.startsWith("MINUS"))
                        && multiplikativni.name.startsWith("<multiplikativni_izraz>")) {

                    aditivni.getSvojstva();
                    if (!Check.canCastImplicit(aditivni.getTip(), Type.INT))
                        error();
                    multiplikativni.getSvojstva();
                    if (!Check.canCastImplicit(multiplikativni.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
