package helper.izrazi;

import helper.Type;

public class izraz_naredba extends Node {
    public izraz_naredba(String name) {
        super(name);
    }

    @Override
    public void getSvojstva() {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("TOCKAZAREZ")) {
                    setTip(Type.INT);
                } else
                    error();
                break;
            }
            case 2: {
                Node izraz = children.get(0);
                if (izraz.name.startsWith("<izraz>") && children.get(1).name.startsWith("TOCKAZAREZ")) {
                    izraz.getSvojstva();
                    setTip(izraz.getTip());
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
