package helper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FuncTable {
    Set<Function> functions;

    public FuncTable() {
        functions = new HashSet<>();
    }

    public Function getByName(String name) {
        for (Function f : functions) {
            if (f.getName().equals(name)) {
                return f;
            }
        }
        return null;
    }

    public Function getByData(String name, Type returnType, List<Type> parameterTypes) {
        for (Function f : functions) {
            if (f.getName().equals(name)
                    && f.getReturnType().equals(returnType)
                    && f.getParameterTypes().equals(parameterTypes)) {
                return f;
            }
        }
        return null;
    }

    public void addFunction(String name, Type returnType, List<Type> parameterTypes) {
        functions.add(new Function(name, returnType, parameterTypes));
    }

    public void addFunction(Function function) {
        functions.add(function);
    }

    public boolean hasDefinedFunction(String name) {
        for (Function f : functions) {
            if (f.getName().equals(name) && f.isDefined()) {
                return true;
            }
        }
        return false;
    }

    public Set<Function> getFunctions() {
        return functions;
    }
}
