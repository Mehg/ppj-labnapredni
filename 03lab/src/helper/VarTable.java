package helper;

import helper.izrazi.ListVrijednosti;

import java.util.ArrayList;
import java.util.List;

public class VarTable {
    List<Variable> variables;

    public VarTable() {
        variables = new ArrayList<>();
    }

    public boolean contains(Variable var) {
        return variables.contains(var);
    }

    public boolean containsByName(String name) {
        for (Variable v : variables) {
            if (v.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }

    public Variable get(int index) {
        return variables.get(index);
    }

    public Variable getByName(String name) {
        for (Variable v : variables) {
            if (v.getName().equals(name)) {
                return v;
            }
        }
        return null;
    }

    public void addVar(Type type, String name, Object value) {
        variables.add(new Variable(type, name, value));
    }

    public void addVar(Type type, String name, Object value, int arraySize) {
        variables.add(new Variable(type, name, value, arraySize));
    }

    public void addVar(ListVrijednosti list) {
        variables.add(new Variable(list));
    }

    public void addVar(Variable variable) {
        variables.add(variable);
    }

    // ok ovaj dolje se cini fucking suvisan al svejedno ga ostavljam
    public void addVar(Variable variable, boolean l_izraz) {
        variable.setL_izraz(l_izraz);
        variables.add(variable);
    }

    public void addVar(Type type, String name, Object value, boolean l_izraz) {
        variables.add(new Variable(type, name, value, l_izraz));
    }

    public void addVar(ListVrijednosti list, boolean l_izraz) {
        variables.add(new Variable(list, l_izraz));
    }

}
