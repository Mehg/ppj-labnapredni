package helper;

import helper.izrazi.ListVrijednosti;

import java.util.Objects;

public class Variable {
    private Type type;
    private String name;
    private Object value;
    private boolean l_izraz;
    private int brElem;
    private Object[] values;

    public Variable(Type type, String name, Object value) {
        this.type = type;
        this.name = name;
        this.value = value;
        l_izraz = false;
    }

    public Variable(Type type, String name, Object value, int arraySize) {
        this.type = type;
        this.name = name;
        this.value = value;
        this.brElem = arraySize;
        l_izraz = false;
    }

    public Variable(Type type, String name, Object value, boolean l_vrijednost) {
        this.type = type;
        this.name = name;
        this.value = value;
        l_izraz = l_vrijednost;
    }

    public Variable(Type type, String name) {
        this(type, name, null);
    }

    public Variable() {
    }

    public Variable(ListVrijednosti l) {
        this.l_izraz = l.getL_izraz();
        this.type = l.getTip();
        this.name = l.getValue();
        this.brElem = l.getBrElem();
    }

    public Variable(ListVrijednosti l, boolean l_izraz) {
        this.l_izraz = l.getL_izraz();
        this.type = l.getTip();
        this.name = l.getValue();
        this.brElem = l.getBrElem();
        this.l_izraz = l_izraz;
    }

    public boolean isL_izraz() {
        return l_izraz;
    }

    public void setL_izraz(boolean l_izraz) {
        this.l_izraz = l_izraz;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public int getBrElem() {
        return brElem;
    }

    public void setBrElem(int brElem) {
        this.brElem = brElem;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Variable variable = (Variable) o;
        return l_izraz == variable.l_izraz && type == variable.type && Objects.equals(name, variable.name) && Objects.equals(value, variable.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, value, l_izraz);
    }
}
