package helper;

import helper.izrazi.ListVrijednosti;
import helper.izrazi.Node;

public class Check {
    public static boolean checkInt(ListVrijednosti node) {
        try {
            Integer.parseInt(node.getValue());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean checkZnak(ListVrijednosti node) {
        String value = node.getValue().substring(1, node.getValue().length() - 1);
        if (value.length() > 1)
            return value.equals("\\t") || value.equals("\\n") || value.equals("\\0")
                    || value.equals("\\'") || value.equals("\\\"") || value.equals("\\\\");
        return !value.contains("\\");
    }

    public static boolean checkNizZnakova(ListVrijednosti node) {
        char[] array = node.getValue().toCharArray();
        for (int i = 0; i < array.length; i++) {
            if (i == 0 || i == array.length - 1) {
                if (array[i] == '\"')
                    continue;
                else if (array[i - 1] == '\\' && array[i] == '\"') {
                    i += 1;
                    continue;
                }
                return false;
            } else if (array[i] == '\\' && (i + 1) != array.length) {
                if (array[i + 1] == 't' || array[i + 1] == 'n' || array[i + 1] == '0'
                        || array[i + 1] == '\'' || array[i + 1] == '\"' || array[i + 1] == '\\')
                    i += 1;
                else return false;
            } else {
                try {
                    int value = (array[i]);
                    if (value < 0 || value > 255)
                        return false;
                } catch (Exception e) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int countNizZnakova(String text){
        text = text.replace("\\", "");
        text = text.substring(1, text.length()-1);
        return text.length();
    }

    public static boolean canCastImplicit(Type type, Type other) {
        if (type.equals(other))
            return true;
        else if (type.equals(Type.CONST_INT) && other.equals(Type.INT))
            return true;
        else if (type.equals(Type.CONST_CHAR) && other.equals(Type.CHAR))
            return true;
        else if (type.equals(Type.INT) && other.equals(Type.CONST_INT))
            return true;
        else if (type.equals(Type.CHAR) && other.equals(Type.CONST_CHAR))
            return true;
        else if (type.equals(Type.CHAR) && other.equals(Type.INT))
            return true;
        else if (canCastImplicitToChar(type) && canCastImplicitToInt(other))
            return true;
        else if (type.equals(Type.NIZ_CHAR) && other.equals(Type.NIZ_CONST_CHAR))
            return true;
        else return type.equals(Type.NIZ_INT) && other.equals(Type.NIZ_CONST_INT);
    }

    public static boolean canCastImplicitToChar(Type type) {
        if (type.equals(Type.CHAR))
            return true;
        else return type.equals(Type.CONST_CHAR);
    }

    public static boolean canCastImplicitToInt(Type type) {
        if (type.equals(Type.INT))
            return true;
        else if (type.equals(Type.CONST_INT))
            return true;
        else return type.equals(Type.CHAR);
    }

    public static boolean canCastImplicitTip(Node node, Type other) {
        return canCastImplicit(node.getTip(), other);
    }

    public static boolean canCastExplicit(Type type, Type other) {
        if (canCastImplicit(type, Type.INT) && canCastImplicit(other, Type.CHAR))
            return true;
        else if (canCastImplicit(type, Type.CHAR) && canCastImplicit(other, Type.INT))
            return true;
        else return canCastImplicit(type, other);
    }

    public static Type makeConst(Type type) {
        switch (type) {
            case NIZ_CHAR: {
                return Type.NIZ_CONST_CHAR;
            }
            case CHAR: {
                return Type.CONST_CHAR;
            }
            case NIZ_INT: {
                return Type.NIZ_CONST_INT;
            }
            case INT: {
                return Type.CONST_INT;
            }
            default:
                return type;
        }
    }

    public static Type removeConst(Type type) {
        switch (type) {
            case NIZ_CONST_CHAR: {
                return Type.NIZ_CHAR;
            }
            case CONST_CHAR: {
                return Type.CHAR;
            }
            case NIZ_CONST_INT: {
                return Type.NIZ_INT;
            }
            case CONST_INT: {
                return Type.INT;
            }
            default:
                return type;
        }
    }

    public static Type removeNiz(Type type) {
        switch (type) {
            case NIZ_INT: {
                return Type.INT;
            }
            case NIZ_CONST_INT: {
                return Type.CONST_INT;
            }
            case NIZ_CHAR: {
                return Type.CHAR;
            }
            case NIZ_CONST_CHAR: {
                return Type.CONST_CHAR;
            }
        }
        return null;
    }

    public static Type makeNiz(Type type) {
        switch (type) {
            case INT: {
                return Type.NIZ_INT;
            }
            case CONST_INT: {
                return Type.NIZ_CONST_INT;
            }
            case CHAR: {
                return Type.NIZ_CHAR;
            }
            case CONST_CHAR: {
                return Type.NIZ_CONST_CHAR;
            }
        }
        return null;
    }

}
