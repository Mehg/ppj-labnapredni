package helper;

import helper.izrazi.ListVrijednosti;
import helper.izrazi.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VarTable {
    List<Variable> variables;
    Map<String, Integer> addresses;

    public VarTable() {
        variables = new ArrayList<>();
        addresses = new HashMap<>();
    }

    public boolean contains(Variable var) {
        return variables.contains(var);
    }

    public boolean containsByName(String name) {
        for (Variable v : variables) {
            if (v.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public Variable get(int index) {
        return variables.get(index);
    }

    public Variable getByName(String name) {
        for (Variable v : variables) {
            if (v.getName().equals(name)) {
                return v;
            }
        }
        return null;
    }

    public void addVar(Type type, String name, Object value, Node context) {
        variables.add(new Variable(type, name, value));
        if (!addresses.containsKey(name)) {
            addresses.put(name, context.getAndIncrementVariableAddress());
        }
    }

    public void addVar(Type type, String name, Object value, int arraySize, Node context) {
        variables.add(new Variable(type, name, value, arraySize));
        if (!addresses.containsKey(name)) {
            addresses.put(name, context.getAndIncrementVariableAddress());
        }
    }

    public void addVar(ListVrijednosti list) {
        variables.add(new Variable(list));
    }

    public void addVar(Variable variable) {
        variables.add(variable);
    }

    // ok ovaj dolje se cini fucking suvisan al svejedno ga ostavljam
    public void addVar(Variable variable, boolean l_izraz) {
        variable.setL_izraz(l_izraz);
        variables.add(variable);
    }

    public void addVar(Type type, String name, Object value, boolean l_izraz, Node context) {
        variables.add(new Variable(type, name, value, l_izraz));
        if (!addresses.containsKey(name)) {
            addresses.put(name, context.getAndIncrementVariableAddress());
        }
    }

    public void addVar(ListVrijednosti list, boolean l_izraz) {
        variables.add(new Variable(list, l_izraz));
    }

    public int locationOf(String name) {
        return addresses.getOrDefault(name, -1);
    }
}
