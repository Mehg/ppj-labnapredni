package helper;

import java.util.HashMap;
import java.util.Map;

public class AssemblyOutput {
    private StringBuilder main;
    private Map<String, StringBuilder> functions;
    private static AssemblyOutput instance = new AssemblyOutput();

    private AssemblyOutput() {
        main = new StringBuilder();
        functions = new HashMap<>();
    }

    public static AssemblyOutput get() {
        return instance;
    }

    public void addFunction(String name) {
        if (!functions.containsKey(name)) {
            functions.put(name, new StringBuilder());
        }
    }

    public StringBuilder getMain() {
        return main;
    }

    public void setMain(StringBuilder main) {
        this.main = main;
    }

    public Map<String, StringBuilder> getFunctions() {
        return functions;
    }

    public StringBuilder getFunction(String name) {
        return functions.get(name);
    }

    public void setFunctions(Map<String, StringBuilder> functions) {
        this.functions = functions;
    }

    public String finalizeOutput() {
        for (String func : functions.keySet()) {
            main.append("\n").append(func).append("\n").append(functions.get(func)).append("\n");
        }
        return "F_MAIN\n" + main.toString();
    }
}
