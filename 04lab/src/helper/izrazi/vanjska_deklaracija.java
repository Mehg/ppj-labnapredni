package helper.izrazi;

import helper.AssemblyOutput;

public class vanjska_deklaracija extends Node {
    public vanjska_deklaracija(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<definicija_funkcije>") || child.name.startsWith("<deklaracija>")) {
                    child.getSvojstva(output);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
