package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Type;

public class multiplikativni_izraz extends Node {
    public multiplikativni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<cast_izraz>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node multiplikativni = children.get(0);
                Node cast = children.get(2);
                if (multiplikativni.name.startsWith("<multiplikativni_izraz>")
                        && (children.get(1).name.startsWith("OP_PUTA")
                        || children.get(1).name.startsWith("OP_DIJELI")
                        || children.get(1).name.startsWith("OP_MOD"))
                        && cast.name.startsWith("<cast_izraz>")) {
                    multiplikativni.getSvojstva(output);

                    if (!Check.canCastImplicit(multiplikativni.getTip(), Type.INT))
                        error();
                    cast.getSvojstva(output);
                    if (!Check.canCastImplicit(cast.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);

                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
