package helper.izrazi;

import helper.AssemblyOutput;
import helper.FuncTable;
import helper.VarTable;

public class prijevodna_jedinica extends Node {
    private VarTable vars;
    private FuncTable funcs;

    public prijevodna_jedinica(String name) {
        super(name);
        setScope(this);
        setRoot(this);
        vars = new VarTable();
        funcs = new FuncTable();
        setLastVariableAddress(10000);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        setCurrentFunctionName("");

        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<vanjska_deklaracija>")) {
                    child.getSvojstva(output);
                } else
                    error();
                break;
            }
            case 2: {
                Node prijevodna = children.get(0);
                Node vanjska = children.get(1);
                if (prijevodna.name.startsWith("<prijevodna_jedinica>") && vanjska.name.startsWith("<vanjska_deklaracija>")) {
                    prijevodna.getSvojstva(output);
                    vanjska.getSvojstva(output);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }

    @Override
    public VarTable getVarTable() {
        return vars;
    }

    @Override
    public FuncTable getFuncTable() {
        return funcs;
    }
}
