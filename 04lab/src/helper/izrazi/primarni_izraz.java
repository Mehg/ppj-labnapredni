package helper.izrazi;

import helper.*;

public class primarni_izraz extends Node {

    public primarni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("IDN")) {
                    Node current = getScope();
                    boolean found = false;

                    do {
                        Function func = current.getFuncTable().getByName(((ListVrijednosti) child).getValue());
                        Variable var = current.getVarTable().getByName(((ListVrijednosti) child).getValue());

                        if (func != null) {
                            setL_izraz(false);
                            setTip(Type.FUNCTION);
                            setPov(func.getReturnType());
                            setTipovi(func.getParameterTypes());
                            found = true;
                            setCallName(func.getName());
                            setDataType(FUNCTION);
//                            if (!func.getParameterTypes().contains(Type.VOID)) {
//                                Node annoying = parent;
//                                // annoying sto identifikator funkcije i zagrade nisu na istoj razini
//                                while (!(annoying.children.get(0) instanceof postfiks_izraz)) {
//                                    annoying = annoying.getParent();
//                                }
//                                for (int i = 0; i < func.getParameterTypes().size(); i++) {
//                                    String argument = annoying.children.get()
//                                    output.getMain().append("\t\tPUSH ")
//                                            .append(argument)
//                                            .append("\n");
//                                }
//                            }
//                            output.getMain().append("\t\tCALL ").append(func.getName()).append("\n");
                            break;
                        } else if (var != null) {
                            setTip(var.getType());
                            setL_izraz(var.isL_izraz());
                            found = true;
                            StringBuilder sb = output.getFunction(getScope().getCurrentFunctionName());
                            if (getScope().getCurrentFunctionName().equals("main")) {
                                sb = output.getMain();
                            }

                            Node currentScope = getScope();
                            String name = getScope().getCurrentFunctionName();
                            Function f = currentScope.getFuncTable().getByName(name);
                            while (f == null) {
                                if (currentScope.parent == null) {
                                    throw new IllegalStateException("Function " + name + " not found!");
                                }
                                currentScope = currentScope.parent.getScope();
                                f = currentScope.getFuncTable().getByName(name);
                            }

                            if (f.getParameterNames().contains(var.getName())) {
                                int offset = f.getParameterNames().size()
                                        - f.getParameterNames().indexOf(var.getName());
                                offset *= 4;
                                sb.append("\t\tLOAD R3, (R7+")
                                        .append(offset)
                                        .append(")\n");
                                sb.append("\t\tPUSH R3\n");
                            } else {

                            int location = getScope().getVarTable().locationOf(var.getName());
                            Node scope = getScope();
                            while (location == -1) {
                                if (getScope().parent == null) {
                                    throw new IllegalStateException("Non-existent variable!");
                                }
                                scope = getScope().parent.getScope();
                                location = scope.getVarTable().locationOf(var.getName());
                            }

                            sb.append("\t\tLOAD ")
                                    .append("R3, (")
                                    .append(location)
                                    .append(")\n");
                            sb.append("\t\tPUSH R3\n");
                            setDataType(VARIABLE);
                            }
                            break;
                        }
                        if (current.parent == null) {
                            break;
                        }
                        current = current.parent.getScope();
                    } while (current != null);

                    if (!found) {
                        error();
                    }

                } else if (child.name.startsWith("BROJ")) {
                    setTip(Type.INT);
                    setL_izraz(false);
                    if (!Check.checkInt((ListVrijednosti) child)) {
                        error();
                    }
                    // TODO provjeriti je li broj prevelik tj stane li u 20 bitova
                    StringBuilder sb = output.getFunction(getScope().getCurrentFunctionName());
                    if (getScope().getCurrentFunctionName().equals("main")
                            || getScope().getCurrentFunctionName().isBlank()) {
                        sb = output.getMain();
                    }

                    sb.append("\t\tMOVE %D ")
                            .append(((ListVrijednosti)child).getValue())
                            .append(", R3\n");
//                    sb.append("\t\tPOP R4\n");
                    sb.append("\t\tPUSH R3\n");
//                    sb.append("\t\tPUSH R4\n");
                    setDataType(VALUE);
                } else if (child.name.startsWith("ZNAK")) {
                    setTip(Type.CHAR);
                    setL_izraz(false);
                    if (!Check.checkZnak((ListVrijednosti) child)) {
                        error();
                    }
                    int ascii = ((ListVrijednosti)child).getValue().toCharArray()[0];
                    StringBuilder sb = output.getFunction(getScope().getCurrentFunctionName());
                    if (getScope().getCurrentFunctionName().equals("main")) {
                        sb = output.getMain();
                    }
                    sb.append("\t\tMOVE %D ")
                            .append(ascii)
                            .append(", R3\n");
                    sb.append("\t\tPUSH R3\n");
                    setDataType(VALUE);
                } else if (child.name.startsWith("NIZ_ZNAKOVA")) {
                    setTip(Type.NIZ_CONST_CHAR);
                    setL_izraz(false);
                    if (!Check.checkNizZnakova((ListVrijednosti) child))
                        error();
                    // TODO jos je unsupported, ne znam kako
                    setDataType(ARRAY);
                } else
                    error();
                break;
            }
            case 3: {
                if (children.get(0).name.startsWith("L_ZAGRADA") && children.get(1).name.startsWith("<izraz>") &&
                        children.get(2).name.startsWith("D_ZAGRADA")) {
                    children.get(1).getSvojstva(output);
                    setTip(children.get(1).getTip());
                    setL_izraz(children.get(1).getL_izraz());
                    setDataType(children.get(1).getDataType());
                    setCallName(children.get(1).getCallName());
                } else {
                    error();
                }
                break;
            }
            default:
                error();
        }
    }
}
