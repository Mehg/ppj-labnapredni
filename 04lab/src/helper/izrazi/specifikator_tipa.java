package helper.izrazi;

import helper.AssemblyOutput;
import helper.Type;

public class specifikator_tipa extends Node {
    public specifikator_tipa(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        Node child = children.get(0);
        if (child.name.startsWith("KR_VOID")) {
            setTip(Type.VOID);
        } else if (child.name.startsWith("KR_CHAR")) {
            setTip(Type.CHAR);
        } else if (child.name.startsWith("KR_INT")) {
            setTip(Type.INT);
        }else
            error();
    }
}
