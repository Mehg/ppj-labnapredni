package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Type;

public class aditivni_izraz extends Node {
    public aditivni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<multiplikativni_izraz>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node aditivni = children.get(0);
                Node multiplikativni = children.get(2);

                if (aditivni.name.startsWith("<aditivni_izraz>")
                        && (children.get(1).name.startsWith("PLUS")
                        || children.get(1).name.startsWith("MINUS"))
                        && multiplikativni.name.startsWith("<multiplikativni_izraz>")) {

                    aditivni.getSvojstva(output);
                    if (!Check.canCastImplicit(aditivni.getTip(), Type.INT))
                        error();
                    multiplikativni.getSvojstva(output);
                    if (!Check.canCastImplicit(multiplikativni.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    StringBuilder sb = output.getFunction(getScope().getCurrentFunctionName());
                    if (getScope().getCurrentFunctionName().equals("main")
                            || getScope().getCurrentFunctionName().isBlank()) {
                        sb = output.getMain();
                    }
                    sb.append("\t\tPOP R0\n\t\tPOP R1\n\t\t");
                    if (children.get(1).name.startsWith("PLUS")) {
                        sb.append("ADD ");
                    } else {
                        sb.append("SUB ");
                    }
                    sb.append("R1, R0, R2\n");
                    sb.append("\t\tPUSH R2\n");
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
