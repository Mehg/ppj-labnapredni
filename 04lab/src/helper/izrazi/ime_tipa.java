package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Type;

public class ime_tipa extends Node {
    public ime_tipa(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<specifikator_tipa>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                } else
                    error();
                break;
            }
            case 2: {
                Node specifikator = children.get(1);
                if (children.get(0).name.startsWith("KR_CONST")
                        && specifikator.name.startsWith("<specifikator_tipa>")) {
                    specifikator.getSvojstva(output);
                    if (specifikator.getTip().equals(Type.VOID))
                        error();

                    setTip(Check.makeConst(specifikator.getTip()));
                }
                break;
            }
            default:
                error();
        }
    }
}
