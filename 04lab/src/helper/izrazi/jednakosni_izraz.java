package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Type;

public class jednakosni_izraz extends Node {
    public jednakosni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<odnosni_izraz>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }

            case 3: {
                Node jednakosni = children.get(0);
                Node odnosni = children.get(2);
                Node op = children.get(1);

                if (jednakosni.name.startsWith("<jednakosni_izraz>")
                        && (op.name.startsWith("OP_EQ")
                        || op.name.startsWith(" OP_NEQ"))
                        && odnosni.name.startsWith("<odnosni_izraz>")) {

                    jednakosni.getSvojstva(output);
                    if (!Check.canCastImplicit(jednakosni.getTip(), Type.INT))
                        error();
                    odnosni.getSvojstva(output);
                    if (!Check.canCastImplicit(odnosni.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
