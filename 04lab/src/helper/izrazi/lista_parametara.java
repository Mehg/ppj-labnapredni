package helper.izrazi;

import helper.AssemblyOutput;

import java.util.ArrayList;

public class lista_parametara extends Node {
    public lista_parametara(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<deklaracija_parametra>")) {
                    child.getSvojstva(output);
                    setTipovi(new ArrayList<>() {{
                        add(child.getTip());
                    }});
                    setImena(new ArrayList<>() {{
                        add(child.getIme());
                    }});

                } else
                    error();
                break;
            }
            case 3: {
                Node listaParametara = children.get(0);
                Node deklaracijaParametara = children.get(2);

                if (listaParametara.name.startsWith("<lista_parametara>")
                        && children.get(1).name.startsWith("ZAREZ")
                        && deklaracijaParametara.name.startsWith("<deklaracija_parametra>")) {
                    listaParametara.getSvojstva(output);
                    deklaracijaParametara.getSvojstva(output);
                    if (listaParametara.getImena().contains(deklaracijaParametara.getIme()))
                        error();

                    setTipovi(new ArrayList<>(listaParametara.getTipovi()));
                    getTipovi().add(deklaracijaParametara.getTip());
                    setImena(new ArrayList<>(listaParametara.getImena()));
                    getImena().add(deklaracijaParametara.getIme());
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
