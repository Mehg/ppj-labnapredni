package helper.izrazi;

import helper.AssemblyOutput;

public class lista_naredbi extends Node {
    public lista_naredbi(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<naredba>")) {
                    child.getSvojstva(output);
                } else
                    error();
                break;
            }
            case 2: {
                Node listaNaredbi = children.get(0);
                Node naredba = children.get(1);
                if (listaNaredbi.name.startsWith("<lista_naredbi>") && naredba.name.startsWith("<naredba>")) {
                    listaNaredbi.getSvojstva(output);
                    naredba.getSvojstva(output);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
