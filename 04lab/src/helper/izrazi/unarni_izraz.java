package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Type;

public class unarni_izraz extends Node {
    public unarni_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<postfiks_izraz>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 2: {
                Node unarni = children.get(1);
                if ((children.get(0).name.startsWith("OP_INC")
                        || children.get(0).name.startsWith("OP_DEC"))
                        && unarni.name.startsWith("<unarni_izraz>")) {
                    unarni.getSvojstva(output);
                    if (!unarni.getL_izraz() || !Check.canCastImplicitTip(unarni, Type.INT))
                        error();
                    setTip(Type.INT);
                    setL_izraz(false);

                } else if (children.get(0).name.startsWith("<unarni_operator>")
                        && children.get(1).name.startsWith("<cast_izraz>")) {
                    Node cast = children.get(1);
                    cast.getSvojstva(output);
                    if (!Check.canCastImplicitTip(cast, Type.INT))
                        error();

                    setTip(Type.INT);
                    setL_izraz(false);
                    if (children.get(0).children.get(0).getName().contains("MINUS")) {
                        StringBuilder sb = output.getFunction(getScope().getCurrentFunctionName());
                        if (getScope().getCurrentFunctionName().equals("main")
                                || getScope().getCurrentFunctionName().isBlank()) {
                            sb = output.getMain();
                        }
                        sb.append("\t\tPOP R3\n\t\tMOVE 0, R4\n\t\tSUB R4, R3, R3\n\t\tPUSH R3\n");
                    }

                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
