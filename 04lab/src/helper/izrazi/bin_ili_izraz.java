package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Type;

public class bin_ili_izraz extends Node {
    public bin_ili_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<bin_xili_izraz>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node ili = children.get(0);
                Node xili = children.get(2);

                if (ili.name.startsWith("<bin_ili_izraz>")
                        && children.get(1).name.startsWith("OP_BIN_ILI")
                        && xili.name.startsWith("<bin_xili_izraz>")) {

                    ili.getSvojstva(output);
                    if (!Check.canCastImplicit(ili.getTip(), Type.INT))
                        error();
                    xili.getSvojstva(output);
                    if (!Check.canCastImplicit(xili.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    StringBuilder sb = output.getMain();
                    sb.append("\t\tPOP R0\n\t\tPOP R1\n\t\t");
                    sb.append("OR R1, R0, R2\n");
                    sb.append("\t\tPUSH R2\n");
                    setL_izraz(false);
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
