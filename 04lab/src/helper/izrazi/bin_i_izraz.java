package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Type;

public class bin_i_izraz extends Node {
    public bin_i_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<jednakosni_izraz>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node binI = children.get(0);
                Node jedn = children.get(2);

                if (binI.name.startsWith("<bin_i_izraz>")
                        && children.get(1).name.startsWith("OP_BIN_I")
                        && jedn.name.startsWith("<jednakosni_izraz>")) {
                    binI.getSvojstva(output);
                    if (!Check.canCastImplicit(binI.getTip(), Type.INT))
                        error();
                    jedn.getSvojstva(output);
                    if (!Check.canCastImplicit(jedn.getTip(), Type.INT))
                        error();
                    setTip(Type.INT);
                    StringBuilder sb = output.getMain();
                    sb.append("\t\tPOP R0\n\t\tPOP R1\n\t\t");
                    sb.append("AND R1, R0, R2\n");
                    sb.append("\t\tPUSH R2\n");
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
