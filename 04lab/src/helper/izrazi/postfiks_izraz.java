package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Function;
import helper.Type;

import java.util.List;

public class postfiks_izraz extends Node {
    public postfiks_izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<primarni_izraz>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                    setTipovi(child.getTipovi());
                    setPov(child.getPov());
                    setCallName(child.getCallName());
                } else
                    error();
                break;
            }
            case 2: {
                Node child = children.get(0);

                if (!child.name.startsWith("<postfiks_izraz>")) {
                    error();
                }
                setCallName(child.getCallName());

                Node secondChild = children.get(1);
                if (secondChild.name.startsWith("OP_INC") || secondChild.name.startsWith("OP_DEC")) {
                    child.getSvojstva(output);

                    if (!child.getL_izraz() || !Check.canCastImplicitTip(child, Type.INT))
                        error();

                    setTip(Type.INT);
                    setL_izraz(false);
                    
                } else {
                    error();
                }
                break;
            }
            case 3: {
                Node child = children.get(0);

                if (child.name.startsWith("<postfiks_izraz>") &&
                        children.get(1).name.startsWith("L_ZAGRADA")
                        && children.get(2).name.startsWith("D_ZAGRADA")) {
                    child.getSvojstva(output);

                    if (child.getTip() != Type.FUNCTION || !child.getTipovi().isEmpty())
                        error();

                    setTip(child.getPov());

                    // todo asm

                    output.getMain().append("\t\tCALL ").append(child.getCallName()).append("\n");

                    setDataType(FUNCTION);

                    setL_izraz(false);
                } else {
                    error();
                }
                break;
            }
            case 4: {
                Node postfix = children.get(0);
                Node izraz = children.get(2);

                if (postfix.name.startsWith("<postfiks_izraz>") &&
                        children.get(1).name.startsWith("L_UGL_ZAGRADA")
                        && izraz.name.startsWith("<izraz>")
                        && children.get(3).name.startsWith("D_UGL_ZAGRADA")) {

                    postfix.getSvojstva(output);

                    if (postfix.getTip() != Type.NIZ_INT &&
                            postfix.getTip() != Type.NIZ_CONST_INT &&
                            postfix.getTip() != Type.NIZ_CHAR &&
                            postfix.getTip() != Type.NIZ_CONST_CHAR)
                        error();
                    izraz.getSvojstva(output);
                    if (!Check.canCastImplicitTip(izraz, Type.INT))
                        error();

                    Type withoutNiz = Check.removeNiz(postfix.getTip());

                    if (withoutNiz == null)
                        error();

                    setTip(withoutNiz);
                    setL_izraz(!withoutNiz.equals(Type.CONST_CHAR) && !withoutNiz.equals(Type.CONST_INT));

                } else if (postfix.name.startsWith("<postfiks_izraz>") &&
                        children.get(1).name.startsWith("L_ZAGRADA")
                        && izraz.name.startsWith("<lista_argumenata>")
                        && children.get(3).name.startsWith("D_ZAGRADA")) {

                    //izraz here acts as lista_argumenata
                    postfix.getSvojstva(output);
                    izraz.getSvojstva(output);

                    List<Type> postfixTipovi = postfix.getTipovi();
                    List<Type> izrazTipovi = izraz.getTipovi();

                    if (postfixTipovi.size() != izrazTipovi.size())
                        error();

                    for (int i = 0; i < postfixTipovi.size(); i++) {
                        if (!Check.canCastImplicit(izrazTipovi.get(i), postfixTipovi.get(i)))
                            error();
                    }

                    if (postfix.getTip() != Type.FUNCTION)
                        error();

                    setTip(postfix.getPov());
                    setL_izraz(false);
                    setDataType(FUNCTION);

//                    System.err.println(getImena());
//
//                    for (Type t : izrazTipovi) {
//
//                    }

//                    System.err.println(postfix.getCallName());
                    setImena(izraz.getImena());
                    Node currentScope = getScope();
                    Function f = currentScope.getFuncTable().getByName(postfix.getCallName());
                    while (f == null) {
                        if (currentScope.parent == null) {
                            throw new IllegalStateException("function not found??");
                        }
                        currentScope = currentScope.parent.getScope();
                        f = currentScope.getFuncTable().getByName(postfix.getCallName());
                    }

                    StringBuilder sb = output.getFunction(getScope().getCurrentFunctionName());
                    if (getScope().getCurrentFunctionName().equals("main")
                            || getScope().getCurrentFunctionName().isBlank()) {
                        sb = output.getMain();
                    }

                    // todo provjeri ovo
//                    for (int i = f.getParameterNames().size() - 1; i >= 0; i--) {
//                        currentScope = getScope();
//                        int paramAddress = currentScope.getVarTable().locationOf(f.getParameterNames().get(i));
//                        while (paramAddress == -1) {
//                            currentScope = currentScope.parent.getScope();
//                            paramAddress = currentScope.getVarTable().locationOf(f.getParameterNames().get(i));
//                        }
//
//                        sb.append("\t\tPOP R0\n\t\tSTORE R0, (")
//                                .append(paramAddress)
//                                .append(")\n");
//                    }


                    sb.append("\t\tCALL ").append(postfix.getCallName()).append("\n");

                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
