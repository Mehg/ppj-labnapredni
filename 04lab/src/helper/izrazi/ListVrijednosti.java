package helper.izrazi;

import helper.AssemblyOutput;
import helper.Type;

public class ListVrijednosti extends Node {
    private int red;
    private String value;

    public ListVrijednosti(String name) {
        super(name.split(" ")[0]);
        String[] parts = name.split(" ");
        red = Integer.parseInt(parts[1]);
        value = parts[2];
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        super.getSvojstva(output);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public Type getTip() {
        return super.getTip();
    }

    @Override
    public boolean getL_izraz() {
        return super.getL_izraz();
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return getName() + "(" + red + "," + value + ")";
    }
}
