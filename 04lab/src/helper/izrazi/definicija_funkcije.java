package helper.izrazi;

import helper.AssemblyOutput;
import helper.Function;
import helper.Type;

import java.util.ArrayList;

public class definicija_funkcije extends Node {

    public definicija_funkcije(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        if (children.size() != 6) {
            error();
        }

        Node imeTipa = children.get(0);
        Node idn = children.get(1);
        Node listaParametara = children.get(3);
        Node slozena_naredba = children.get(5);

        setCurrentFunctionName(((ListVrijednosti) idn).getValue());

        if (imeTipa.getName().startsWith("<ime_tipa>")
                && idn.getName().startsWith("IDN")
                && children.get(2).getName().startsWith("L_ZAGRADA")
                && listaParametara.getName().startsWith("KR_VOID")
                && children.get(4).getName().startsWith("D_ZAGRADA")
                && slozena_naredba.getName().startsWith("<slozena_naredba>")) {

            imeTipa.getSvojstva(output);
            if (imeTipa.getTip().equals(Type.CONST_CHAR) || imeTipa.getTip().equals(Type.CONST_INT)) {
                error();
            }

            if (getScope().getFuncTable().hasDefinedFunction(((ListVrijednosti) idn).getValue())) {
                error();
            }

            Function check = getScope().getFuncTable().getByData(((ListVrijednosti) idn).getValue(), imeTipa.getTip(), new ArrayList<>());


            if (check != null
                    && (!check.getReturnType().equals(imeTipa.getTip())
                    || !check.getParameterTypes().isEmpty())) {
                error();
            } else if (check != null) {
                getScope().getFuncTable().getByData(((ListVrijednosti) idn).getValue(), imeTipa.getTip(), new ArrayList<>()).setDefined(true);
            } else {
                Function newFunction = new Function(((ListVrijednosti) idn).getValue(), imeTipa.getTip(), new ArrayList<>(), new ArrayList<>());
                getRoot().getFuncTable().addFunction(newFunction);
                newFunction.setDefined(true);
                getScope().getFuncTable().addFunction(newFunction);
            }

            if (!((ListVrijednosti) idn).getValue().equals("main")) {
                output.addFunction(((ListVrijednosti) idn).getValue());
            }

            slozena_naredba.getSvojstva(output);

        } else if (imeTipa.getName().startsWith("<ime_tipa>")
                && idn.getName().startsWith("IDN")
                && children.get(2).getName().startsWith("L_ZAGRADA")
                && listaParametara.getName().startsWith("<lista_parametara>")
                && children.get(4).getName().startsWith("D_ZAGRADA")
                && slozena_naredba.getName().startsWith("<slozena_naredba>")) {

            imeTipa.getSvojstva(output);
            if (imeTipa.getTip().equals(Type.CONST_CHAR) || imeTipa.getTip().equals(Type.CONST_INT)) {
                error();
            }

            Node currentScope = getScope();

            do {
                if (currentScope.getFuncTable().hasDefinedFunction(((ListVrijednosti) idn).getValue())) {
                    error();
                } else {
                    if (currentScope.parent == null)
                        break;
                    currentScope = currentScope.parent.getScope();
                }
            } while (currentScope != null);

            listaParametara.getSvojstva(output);
            setImena(listaParametara.getImena());

            Function check = getScope().getFuncTable().getByData(((ListVrijednosti) idn).getValue(), imeTipa.getTip(), listaParametara.getTipovi());

            if (check != null
                    && !(check.getReturnType().equals(imeTipa.getTip())
                    && check.getParameterTypes().equals(listaParametara.getTipovi())) && check.isDefined()) {
                error();
            } else if (check != null) {
                getScope().getFuncTable().getByData(((ListVrijednosti) idn).getValue(), imeTipa.getTip(), listaParametara.getTipovi()).setDefined(true);
            } else {
                Function newFunction = new Function(((ListVrijednosti) idn).getValue(), imeTipa.getTip(), listaParametara.getTipovi(), listaParametara.getImena());
                getRoot().getFuncTable().addFunction(newFunction);
                newFunction.setDefined(true);
                getScope().getFuncTable().addFunction(newFunction);

            }

            for (int i = 0; i < listaParametara.getTipovi().size(); i++) {
                slozena_naredba.getScope().getVarTable().addVar(listaParametara.getTipovi().get(i), listaParametara.getImena().get(i), null, this);
            }

            slozena_naredba.setTipovi(listaParametara.getTipovi());
            slozena_naredba.setImena(listaParametara.getImena());
            if (!((ListVrijednosti) idn).getValue().equals("main")) {
                output.addFunction(((ListVrijednosti) idn).getValue());
            }
            slozena_naredba.getSvojstva(output);
        } else {
            error();
        }
    }
}
