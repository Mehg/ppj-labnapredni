package helper.izrazi;

import helper.AssemblyOutput;

public class izraz extends Node {
    public izraz(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 1: {
                Node child = children.get(0);
                if (child.name.startsWith("<izraz_pridruzivanja>")) {
                    child.getSvojstva(output);
                    setTip(child.getTip());
                    setL_izraz(child.getL_izraz());
                } else
                    error();
                break;
            }
            case 3: {
                Node izraz = children.get(0);
                Node pridruzivanja = children.get(2);
                if (izraz.name.startsWith("<izraz>")
                        && children.get(1).name.startsWith("ZAREZ")
                        && pridruzivanja.name.startsWith("<izraz_pridruzivanja>")) {
                    izraz.getSvojstva(output);
                    pridruzivanja.getSvojstva(output);
                    setTip(pridruzivanja.getTip());
                    setL_izraz(false);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
