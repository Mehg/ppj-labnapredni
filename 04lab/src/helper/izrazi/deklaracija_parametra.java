package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Type;

public class deklaracija_parametra extends Node {
    public deklaracija_parametra(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 2: {
                Node imeTipa = children.get(0);
                Node idn = children.get(1);
                if (imeTipa.name.startsWith("<ime_tipa>")
                        && idn.name.startsWith("IDN")) {
                    imeTipa.getSvojstva(output);
                    if (imeTipa.getTip().equals(Type.VOID))
                        error();
                    setTip(imeTipa.getTip());
                    setIme(((ListVrijednosti) idn).getValue());

                } else
                    error();
                break;
            }
            case 4: {
                Node imeTipa = children.get(0);
                Node idn = children.get(1);
                if (imeTipa.name.startsWith("<ime_tipa>")
                        && idn.name.startsWith("IDN")
                        && children.get(2).name.startsWith("L_UGL_ZAGRADA")
                        && children.get(3).name.startsWith("D_UGL_ZAGRADA")) {

                    imeTipa.getSvojstva(output);
                    if (imeTipa.getTip().equals(Type.VOID))
                        error();
                    if (Check.makeNiz(imeTipa.getTip()) == null)
                        error();
                    setTip(Check.makeNiz(imeTipa.getTip()));
                    setIme(((ListVrijednosti) idn).getValue());
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
