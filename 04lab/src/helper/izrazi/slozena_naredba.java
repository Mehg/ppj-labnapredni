package helper.izrazi;

import helper.AssemblyOutput;
import helper.FuncTable;
import helper.VarTable;

public class slozena_naredba extends Node {
    private VarTable vars;
    private FuncTable funcs;

    public slozena_naredba(String name) {
        super(name);
        vars = new VarTable();
        funcs = new FuncTable();
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        setCurrentFunctionName(parent.getCurrentFunctionName() == null ? "" : parent.getCurrentFunctionName());

        switch (children.size()) {
            case 3: {
                Node lista_naredbi = children.get(1);
                if (children.get(0).name.startsWith("L_VIT_ZAGRADA")
                        && lista_naredbi.name.startsWith("<lista_naredbi>")
                        && children.get(2).name.startsWith("D_VIT_ZAGRADA")) {
                    lista_naredbi.getSvojstva(output);
                } else
                    error();
                break;
            }
            case 4: {
                Node lista_deklaracija = children.get(1);
                Node lista_naredbi = children.get(2);
                if (children.get(0).name.startsWith("L_VIT_ZAGRADA")
                        && lista_deklaracija.name.startsWith("<lista_deklaracija>")
                        && lista_naredbi.name.startsWith("<lista_naredbi>")
                        && children.get(3).name.startsWith("D_VIT_ZAGRADA")) {
                    lista_deklaracija.getSvojstva(output);
                    lista_naredbi.getSvojstva(output);
                } else
                    error();
                break;
            }
            default:
                error();
        }
    }

    @Override
    public VarTable getVarTable() {
        return vars;
    }

    @Override
    public FuncTable getFuncTable() {
        return funcs;
    }
}
