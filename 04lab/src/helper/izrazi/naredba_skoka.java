package helper.izrazi;

import helper.AssemblyOutput;
import helper.Check;
import helper.Function;
import helper.Type;

public class naredba_skoka extends Node {
    public naredba_skoka(String name) {
        super(name);
    }

    @Override
    public void getSvojstva(AssemblyOutput output) {
        switch (children.size()) {
            case 2: {
                Node first = children.get(0);
                Node second = children.get(1);

                if ((first.name.startsWith("KR_CONTINUE") || first.name.startsWith("KR_BREAK"))
                        && second.name.startsWith("TOCKAZAREZ")) {

                    Node current = this;
                    boolean hasFor = false;
                    while (current.parent != null) {
                        if (current.parent.getName().startsWith("<naredba_petlje>")) {
                            hasFor = true;
                            break;
                        }
                        current = current.parent;
                    }

                    if (!hasFor)
                        error();

                } else if (first.name.startsWith("KR_RETURN") && second.name.startsWith("TOCKAZAREZ")) {
                    Node current = this.parent;
                    boolean hasRightFunc = false;
                    while (current != null) {
                        if (current.name.startsWith("<definicija_funkcije>") && current.children.get(0).getTip().equals(Type.VOID)) {
                            hasRightFunc = true;
                            StringBuilder sb = output.getFunction(current.getCurrentFunctionName());
                            if (current.getCurrentFunctionName().equals("main")) {
                                sb = output.getMain();
                            }
                            sb.append("\t\tRET\n");
                            break;
                        }
                        current = current.parent;
                    }
                    if (!hasRightFunc)
                        error();
                } else
                    error();
                break;
            }
            case 3: {
                Node izraz = children.get(1);
                if (children.get(0).name.startsWith("KR_RETURN") && izraz.name.startsWith("<izraz>")
                        && children.get(2).name.startsWith("TOCKAZAREZ")) {
                    izraz.getSvojstva(output);

                    Node current = this.parent;
                    boolean hasRightFunc = false;
                    while (current != null) {
                        if (current.name.startsWith("<definicija_funkcije>")) {
                            hasRightFunc = Check.canCastImplicit(izraz.getTip(), current.children.get(0).getTip());
                            StringBuilder sb = output.getFunction(current.getCurrentFunctionName());
                            if (current.getCurrentFunctionName().equals("main")) {
                                sb = output.getMain();
                            }

                            String returnName = "";
                            Node lastChild = children.get(1);
                            while (!(lastChild instanceof primarni_izraz)) {
                                if (lastChild.children.isEmpty()) {
                                    throw new IllegalStateException("<primarni_izraz> not found!");
                                }
                                lastChild = lastChild.children.get(0);
                            }
                            returnName = ((ListVrijednosti) lastChild.children.get(0)).getValue();

                            if (current.getCurrentFunctionName().equals("main")) {
                                sb = output.getMain();
//                                sb.append("\t\tMOVE ");
//                                sb.append(returnName);
//                                sb.append(", R6\n");
                            }
//                            else {
//                                sb.append("\t\tPUSH ");
//                                sb.append(returnName);
//                                sb.append("\n");
//                            }
                            // todo review this ^^^

                            if (((ListVrijednosti) lastChild.children.get(0)).getName().equals("IDN")) {
                                Node currentScope = getScope();

//                                if (izraz.getDataType() == VARIABLE) {
//                                    System.err.println("VAR USPJEH");
//                                    String rememberTheName = returnName;
//                                    returnName = Integer.toString(currentScope.getVarTable().locationOf(rememberTheName));
//                                    while (returnName.equals("-1")) {
//                                        if (currentScope.parent == null) {
//                                            throw new IllegalStateException("No such variable!");
//                                        }
//                                        currentScope = currentScope.parent.getScope();
//                                        returnName = Integer.toString(currentScope.getVarTable().locationOf(rememberTheName));
//                                    }
//                                } else if (izraz.getDataType() == FUNCTION) {
//                                    System.err.println("USPJEH");
//                                }
//                                sb.append("\t\tPOP R6\n");
                            }
//
//                            if (getScope().getCurrentFunctionName().equals("main")){
//                                sb.append("\t\tPOP R6\n");
//                            }

                            Node currentScope = getScope();
                            Function f = currentScope.getFuncTable().getByName(getScope().getCurrentFunctionName());
                            while (f == null) {
                                if (currentScope.parent == null) {
                                    throw new IllegalStateException("Function not found!");
                                }
                                currentScope = currentScope.parent.getScope();
                                f = currentScope.getFuncTable().getByName(getScope().getCurrentFunctionName());
                            }

                            sb.append("\t\tPOP R3\n");
                            sb.append("\t\tPOP R4\n");
                            if (f.getParameterNames().size() > 0) {
                                sb.append("\t\tADD R7, ")
                                        .append(f.getParameterNames().size() * 4)
                                        .append(", R7\n");
                            }
                            sb.append("\t\tPUSH R3\n");
                            sb.append("\t\tPUSH R4\n");

                            sb.append("\t\tRET\n");
                            break;
                        }
                        current = current.parent;
                    }
                    if (!hasRightFunc)
                        error();

                } else
                    error();
                break;
            }
            default:
                error();
        }
    }
}
