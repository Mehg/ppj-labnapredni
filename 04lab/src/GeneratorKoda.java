import helper.AssemblyOutput;
import helper.Function;
import helper.Type;
import helper.izrazi.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class GeneratorKoda {
    public static List<String> lines = new ArrayList<>();

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Stack<Node> nodes = new Stack<>();
        Stack<Integer> indexes = new Stack<>();

        StringBuilder outputCode = new StringBuilder()
                .append("\t\tMOVE 40000, R7\n\t\tCALL F_MAIN\n\t\tPOP R6\n\t\tHALT\n");

        while (sc.hasNext()) {
            String line = sc.nextLine();
            //helper.izrazi.Node node = new helper.izrazi.Node(line.trim());
            Node node = decodeNode(line.trim());
            int index = line.length() - line.stripLeading().length();

            lines.add(line);

            while (!indexes.isEmpty() && index <= indexes.peek()) {
                nodes.pop();
                indexes.pop();
            }

            if (!indexes.isEmpty() && index - indexes.peek() == 1) {
                Node parent = nodes.peek();

                parent.addChild(node);
                node.setParent(parent);
            }

            nodes.push(node);
            indexes.push(index);
        }
        sc.close();

        AssemblyOutput output = AssemblyOutput.get();

        Node root = nodes.get(0);
        root.getSvojstva(output);

        //System.out.println(lines);

        if (!root.getFuncTable().hasDefinedFunction("main")) {
            System.out.println("main");
            System.exit(0);
        }

        Function m = root.getFuncTable().getByName("main");
        if (!m.getParameterTypes().isEmpty() || !m.getReturnType().equals(Type.INT)) {
            System.out.println("main");
            System.exit(0);
        }

        for (helper.Function function : root.getFuncTable().getFunctions()) {
            if (!function.isDefined()) {
                System.out.println("funkcija");
                System.exit(0);
            }
        }

        outputCode.append(output.finalizeOutput());

        System.out.println(outputCode);

    }

    private static Node decodeNode(String name) {
        if (name.startsWith("<aditivni_izraz>")) {
            return new aditivni_izraz(name);
        } else if (name.startsWith("<bin_i_izraz>")) {
            return new bin_i_izraz(name);
        } else if (name.startsWith("<bin_ili_izraz>")) {
            return new bin_ili_izraz(name);
        } else if (name.startsWith("<bin_xili_izraz>")) {
            return new bin_xili_izraz(name);
        } else if (name.startsWith("<cast_izraz>")) {
            return new cast_izraz(name);
        } else if (name.startsWith("<definicija_funkcije>")) {
            return new definicija_funkcije(name);
        } else if (name.startsWith("<deklaracija>")) {
            return new deklaracija(name);
        } else if (name.startsWith("<deklaracija_parametra>")) {
            return new deklaracija_parametra(name);
        } else if (name.startsWith("<ime_tipa>")) {
            return new ime_tipa(name);
        } else if (name.startsWith("<inicijalizator>")) {
            return new inicijalizator(name);
        } else if (name.startsWith("<init_deklarator>")) {
            return new init_deklarator(name);
        } else if (name.startsWith("<izravni_deklarator>")) {
            return new izravni_deklarator(name);
        } else if (name.startsWith("<izraz>")) {
            return new izraz(name);
        } else if (name.startsWith("<izraz_naredba>")) {
            return new izraz_naredba(name);
        } else if (name.startsWith("<izraz_pridruzivanja>")) {
            return new izraz_pridruzivanja(name);
        } else if (name.startsWith("<jednakosni_izraz>")) {
            return new jednakosni_izraz(name);
        } else if (name.startsWith("<lista_argumenata>")) {
            return new lista_argumenata(name);
        } else if (name.startsWith("<lista_deklaracija>")) {
            return new lista_deklaracija(name);
        } else if (name.startsWith("<lista_init_deklaratora>")) {
            return new lista_init_deklaratora(name);
        } else if (name.startsWith("<lista_izraza_pridruzivanja>")) {
            return new lista_izraza_pridruzivanja(name);
        } else if (name.startsWith("<lista_naredbi>")) {
            return new lista_naredbi(name);
        } else if (name.startsWith("<lista_parametara>")) {
            return new lista_parametara(name);
        } else if (name.startsWith("<log_i_izraz>")) {
            return new log_i_izraz(name);
        } else if (name.startsWith("<log_ili_izraz>")) {
            return new log_ili_izraz(name);
        } else if (name.startsWith("<multiplikativni_izraz>")) {
            return new multiplikativni_izraz(name);
        } else if (name.startsWith("<naredba>")) {
            return new naredba(name);
        } else if (name.startsWith("<naredba_grananja>")) {
            return new naredba_grananja(name);
        } else if (name.startsWith("<naredba_petlje>")) {
            return new naredba_petlje(name);
        } else if (name.startsWith("<naredba_skoka>")) {
            return new naredba_skoka(name);
        } else if (name.startsWith("<odnosni_izraz>")) {
            return new odnosni_izraz(name);
        } else if (name.startsWith("<postfiks_izraz>")) {
            return new postfiks_izraz(name);
        } else if (name.startsWith("<prijevodna_jedinica>")) {
            return new prijevodna_jedinica(name);
        } else if (name.startsWith("<primarni_izraz>")) {
            return new primarni_izraz(name);
        } else if (name.startsWith("<slozena_naredba>")) {
            return new slozena_naredba(name);
        } else if (name.startsWith("<specifikator_tipa>")) {
            return new specifikator_tipa(name);
        } else if (name.startsWith("<unarni_izraz>")) {
            return new unarni_izraz(name);
        } else if (name.startsWith("<unarni_operator>")) {
            return new unarni_operator(name);
        } else if (name.startsWith("<vanjska_deklaracija>")) {
            return new vanjska_deklaracija(name);
        } else if (isListVrijednost(name))
            return new ListVrijednosti(name);
        else return new Node(name);
    }

    private static boolean isListVrijednost(String name) {
        return !name.startsWith("<") && !name.contains(">");
    }
}
