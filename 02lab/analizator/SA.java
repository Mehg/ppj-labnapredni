import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class SA {

    public static List<String> nezavrsni;
    public static List<String> zavrsni;
    public static List<String> sinkronizacijski;
    public static ArrayList<String> produkcije;
    public static ArrayList<ArrayList<String>> akcija;
    public static ArrayList<ArrayList<String>> novoStanje;
    public static ArrayList<String> niz;
    public static Stack<Object> stack;
    public static StringBuilder sb = new StringBuilder();


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        niz = new ArrayList<>();
        stack = new Stack<>();

        while (sc.hasNext()) {
            niz.add(sc.nextLine());
        }
        niz.add(";");

        try {
            read();
        } catch (IOException e) {
            System.err.println("IOException");
        }

        izgradiGenerativnoStablo();
        ispisGenerativnogStabla((Node) stack.peek(), 0);
        System.out.println(sb.toString());
    }

    public static void read() throws IOException {
        Path localDir = Paths.get("").toAbsolutePath();
        File dir = new File(String.valueOf(localDir));
        File file = new File(dir, "LibSA.txt");

        RandomAccessFile fw = new RandomAccessFile(file, "rw");
        fw.seek(0);

        String nextLine = fw.readLine();
        nezavrsni = new ArrayList<>(Arrays.asList(nextLine.split(", ")));


        nextLine = fw.readLine();
        zavrsni = new ArrayList<>(Arrays.asList(nextLine.split(", ")));


        nextLine = fw.readLine();
        sinkronizacijski = new ArrayList<>(Arrays.asList(nextLine.split(", ")));

        produkcije = new ArrayList<>();

        nextLine = fw.readLine();
        nextLine = fw.readLine();
        while (!nextLine.equals("Akcija:")) {
            produkcije.add(nextLine);
            nextLine = fw.readLine();
        }

        akcija = new ArrayList<>();
        nextLine = fw.readLine();
        while (!nextLine.equals("NovoStanje:")) {
            ArrayList<String> redak = new ArrayList<>(Arrays.asList(nextLine.split(", ")));
            akcija.add(redak);
            nextLine = fw.readLine();
        }

        novoStanje = new ArrayList<>();
        nextLine = fw.readLine();

        while (nextLine != null) {
            ArrayList<String> redak = new ArrayList<>(Arrays.asList(nextLine.split(", ")));
            novoStanje.add(redak);
            nextLine = fw.readLine();
        }

        fw.close();
    }

    public static void izgradiGenerativnoStablo() {
        stack.push(0);

        int i = 0;
        while (true) {
            String next = niz.get(i);
            String zavrsan = next.split(" ")[0];
            int zavrsniIndex = zavrsni.contains(zavrsan) ? zavrsni.indexOf(zavrsan) : zavrsni.size();
            int stanje = (Integer) stack.peek();

            String stoDalje = akcija.get(stanje).get(zavrsniIndex);

            if (stoDalje.startsWith("p")) {
                //pomakni
                pomakni(next, stoDalje);
                i++;

            } else if (stoDalje.startsWith("r")) {
                //reduciraj
                reduciraj(stoDalje);

            } else if (stoDalje.equals("+") && stack.size() == 3) {
                //prihvati
                stack.pop();
                if (nezavrsni.get(1).equals(((Node) stack.peek()).stavka)) {
                    return;
                }
            } else {
                //oporavak
                ispisGreske(next);

                while (!sinkronizacijski.contains(next.split(" ")[0])) {
                    next = niz.get(i++);
                }

                oporavak(next);

            }
        }
    }

    private static void oporavak(String next) {
        if (next.split(" ")[0].equals(";")) {
            while (akcija.get((Integer) stack.peek()).get(zavrsni.size()).equals("-")) {
                stack.pop();
                stack.pop();
            }
        } else {
            while (akcija.get((Integer) stack.peek()).get(zavrsni.indexOf(next.split(" ")[0])).equals("-")) {
                stack.pop();
                stack.pop();
            }
        }
    }

    private static void ispisGreske(String next) {
        System.err.printf("%10s %50s %50s \n", "broj retka", "ocekivano", "procitano");
        int stanjeNaVrhu = (Integer) stack.peek();

        ArrayList<String> ocekivano = new ArrayList<>(akcija.get(stanjeNaVrhu));
        ArrayList<Integer> ocekivanoBrojvei = new ArrayList<>();
        ocekivano.forEach(s -> {
            if (!s.equals("-"))
                ocekivanoBrojvei.add(ocekivano.indexOf(s));
        });
        ArrayList<String> ocekivaniZavrsni = new ArrayList<>();
        ocekivanoBrojvei.forEach(s -> ocekivaniZavrsni.add(zavrsni.get(s)));

        System.err.printf("%10d %50s %50s \n", next.equals(";") ? niz.size() : Integer.parseInt(next.split(" ")[1]), String.join(",", ocekivaniZavrsni), next.equals(";") ? next : next.split(" ")[0]);
    }

    private static void reduciraj(String stoDalje) {
        int kojaProdukcija = Integer.parseInt(stoDalje.substring(1));
        String produkcija = produkcije.get(kojaProdukcija);

        String lijeva = produkcija.split("->")[0].trim();
        String desna = produkcija.split("->")[1].trim();

        Node nova = new Node(lijeva, null);

        if (!desna.contains("$")) {
            List<Node> nodes = new ArrayList<>();
            for (String s : desna.split(" ")) {
                Integer st = (Integer) stack.pop();
                Node n = (Node) stack.pop();
                nodes.add(n);
            }
            for (int i = nodes.size() - 1; i >= 0; i--) {
                nova.addChild(nodes.get(i));
            }
        } else nova.addChild(new Node("$", null));

        String novo = novoStanje.get((Integer) stack.peek()).get(nezavrsni.indexOf(lijeva));
        stack.push(nova);
        stack.push(Integer.parseInt(novo.substring(1)));
    }

    private static void pomakni(String next, String stoDalje) {
        int sljedeceStanje = Integer.parseInt(stoDalje.substring(1));
        Node n = new Node(next, null);
        stack.push(n);
        stack.push(sljedeceStanje);
    }


    public static void ispisGenerativnogStabla(Node node, int razina) {
        sb.append(" ".repeat(razina));
        sb.append(node.stavka).append("\n");
        if (node.children != null)
            node.children.forEach(c -> ispisGenerativnogStabla(c, razina + 1));
    }

    public static class Node {
        private String stavka;
        private ArrayList<Node> children;

        public Node(String stavka, ArrayList<Node> children) {
            this.stavka = stavka;
            this.children = children;
        }

        public String getStavka() {
            return stavka;
        }

        public void setStavka(String stavka) {
            this.stavka = stavka;
        }

        public ArrayList<Node> getChildren() {
            return children;
        }

        public void setChildren(ArrayList<Node> children) {
            this.children = children;
        }

        public void addChild(Node child) {
            if (this.children == null) this.children = new ArrayList<>();
            this.children.add(child);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return Objects.equals(stavka, node.stavka) &&
                    Objects.equals(children, node.children);
        }

        @Override
        public int hashCode() {
            return Objects.hash(stavka, children);
        }
    }
}
