import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Array;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class GSA {
    /**
     * nezavrsni znakovi gramatike - prvi ce uvijek biti "pocetni" jer ga dodajemo
     */
    public static List<String> nezavrsni;
    /**
     * zavrsni znakovi gramatike
     */
    public static List<String> zavrsni;
    /**
     * sinkronizacijski znakovi
     */
    public static List<String> sinkronizacijski;
    /**
     * produkcije - prva je uvijek pocetni -> prvi definirani u datoteci
     */
    public static Map<String, List<String>> produkcije;
    public static ArrayList<String> produkcijeZaAckijuINovoStanje;

    /**
     * Lista Stavki
     */
    public static ArrayList<String> stavke;
    /**
     * Mapa oblika Stvaka+slijedi -> Lista listi stavki+slijedi u koje prelazi
     * <p>
     * Ako npr imamo produkciju S -> b A a i sve njene stavke
     * <p>
     * Izgled stavki+slijedi:
     * "S -> * b A a [;]"
     * "S -> b * A a [;]"
     * "S -> b A * a [;]"
     * "S -> b A a * [;]"
     * <p>
     * Skica mape prijelazi:<p>
     * asdf                     a                       b                           S       A                       $<p>
     * "S -> * b A a [;]"       []                      ["S -> b * A a [;]"]        []      []                      []<p>
     * "S -> b * A a [;]"       []                      []                          []      ["S -> b A * a [;]"]    ["A->...", "A->..."]<p>
     * "S -> b A * a [;]"       ["S -> b A a * [;]"]    []                          []      []                      []<p>
     * "S -> b A a * [;]"       []                      []                          []      []                      []
     * <p>
     * note: uvijek ce biti poredak zaglavlja redoslijedom nezavrsni znakovi, pa zavrsni pa epsilon<p>
     * note: kada stavka ima zvjezdicu na kraju svi njeni prijelazi su prazne liste
     */
    public static final LinkedHashMap<String, ArrayList<ArrayList<String>>> prijelaziENKA = new LinkedHashMap<>();
    /**
     * Matrica broj nezavrsnih x broj svih znakova - ako je [i][j] true onda nezavrsni[i] zapocinje sa
     * nezavrsni[i] ILI zavrsni[broj nezavrsnih + i] - ovisi je li i >= br. nezavr.
     */
    public static boolean[][] zapocinje;
    /**
     * kome se da pisati, isto ko ovo gore samo za izravni pocetak
     */
    public static boolean[][] zapocinjeIzravno;
    /**
     * Skup stanja koja mogu otic u epsilon oopsie
     */
    public static Set<String> prazni;
    /**
     * Struktura zasluxzna za basic functionality DKA, sastoji se samo od stanja i prijelaza u druga stanja
     */
    public static LinkedHashMap<Integer, ArrayList<Integer>> prijelaziDKA;
    /**
     * Struktura zasluzna za pracenje/prijevod koje se stavke nalaze u kojem stanju DKA
     */
    public static LinkedHashMap<Integer, ArrayList<String>> dkaStavke;

    public static ArrayList<ArrayList<String>> akcija;
    public static ArrayList<ArrayList<String>> novoStanje;


    public static void main(String[] args) {
        read();
        generateENKA();
        generateDKA();
        generateAkcija();
        generateNovoStanje();
        //soutForTesting();
        try {
            write();
        } catch (IOException e) {
            System.err.println("IO Exception");
        }
    }

    private static void read() {
        Scanner sc = new Scanner(System.in);

        nezavrsni = new ArrayList<>();
        nezavrsni.add("pocetni");
        nezavrsni.addAll(Arrays.asList(sc.nextLine().substring(3).split(" ")));
        zavrsni = new ArrayList<>(Arrays.asList(sc.nextLine().substring(3).split(" ")));
        sinkronizacijski = new ArrayList<>(Arrays.asList(sc.nextLine().substring(5).split(" ")));
        produkcije = new LinkedHashMap<>();
        produkcije.put("pocetni", Collections.singletonList(nezavrsni.get(1))); //prva produkcija
        produkcijeZaAckijuINovoStanje = new ArrayList<>();
        produkcijeZaAckijuINovoStanje.add("pocetni -> " + nezavrsni.get(1));

        String key = "";
        while (sc.hasNext()) {
            String next = sc.nextLine();

            if (next.startsWith(" ")) {
                //desna strana produkcije
                List<String> value = produkcije.get(key);
                value.add(next.substring(1));
                produkcijeZaAckijuINovoStanje.add(key + " -> " + next.substring(1));
            } else {
                //lijeva strana produkcije
                key = next;
                if (!produkcije.containsKey(key))
                    produkcije.put(next, new ArrayList<>());
            }
        }

    }

    private static void generateENKA() {
        izracunajPrazne();
        izracunajZapocinjeIzravno();
        izracunajZapocinje();
        dodajStavke();
        dodajPrijelaze(stavke.get(0), new ArrayList<>() {{
            add(";");
        }});

    }

    private static void dodajStavke() {
        stavke = new ArrayList<>();
        boolean isEpsilon = false;

        for (String lijeva : produkcije.keySet()) {
            for (String desna : produkcije.get(lijeva)) {
                String[] desnaDijelovi = desna.split(" ");

                for (int i = 0; i <= desnaDijelovi.length; i++) {
                    ArrayList<String> stavkaDijelovi = new ArrayList<>(Arrays.asList(desnaDijelovi));

                    if (stavkaDijelovi.contains("$")) {
                        stavkaDijelovi.clear();
                        stavkaDijelovi.add("*");
                        isEpsilon = true;
                    } else {
                        stavkaDijelovi.add(i, "*");
                    }

                    StringBuilder sb = new StringBuilder();
                    sb.append(lijeva).append(" -> ");
                    stavkaDijelovi.forEach(s -> sb.append(s).append(" "));

                    stavke.add(sb.toString());
                    if (isEpsilon) {
                        isEpsilon = false;
                        break;
                    }
                }
            }
        }
    }

    public static void dodajPrijelaze(String stavka, ArrayList<String> slijedi) {
        String prva = stavka + slijedi;

        String desna = stavka.split(" -> ")[1];
        ArrayList<ArrayList<String>> zaSveZnakove = prijelaziENKA.containsKey(prva) ? prijelaziENKA.get(prva) : new ArrayList<>();
        ArrayList<String> zaJedanZnak = prijelaziENKA.containsKey(prva) ? prijelaziENKA.get(prva).get(prijelaziENKA.get(prva).size() - 1) : new ArrayList<>();
        if (!zaJedanZnak.isEmpty()) zaSveZnakove.remove(prijelaziENKA.get(prva).size() - 1);
        String nakonZvijezdice = "";

        if (!desna.endsWith("* "))
            nakonZvijezdice = desna.split("\\* ")[1].split(" ")[0];

        if (zaSveZnakove.isEmpty()) {
            for (int i = 0; i < zavrsni.size(); i++) {
                if (nakonZvijezdice.equals(zavrsni.get(i))) {
                    zaSveZnakove.add(new ArrayList<>() {{
                        add(stavke.get(stavke.indexOf(stavka) + 1) + slijedi);
                    }});
                } else {
                    zaSveZnakove.add(new ArrayList<>());
                }
            }

            for (int i = 0; i < nezavrsni.size(); i++) {
                if (nakonZvijezdice.equals(nezavrsni.get(i))) {
                    zaSveZnakove.add(new ArrayList<>() {{
                        add(stavke.get(stavke.indexOf(stavka) + 1) + slijedi);
                    }});
                } else {
                    zaSveZnakove.add(new ArrayList<>());
                }
            }
        }

        if (!desna.endsWith("* ") && nakonZvijezdice.contains("<")) {
            for (String stavka2 : stavke) {
                String lijeva2 = stavka2.split(" -> ")[0];
                String desna2 = stavka2.split(" -> ")[1];

                if (desna2.startsWith("* ") && lijeva2.equals(nakonZvijezdice)) {
                    ArrayList<String> slijedi2 = izracunajSlijedi(prva);
                    String druga = stavka2 + slijedi2;

                    if (!zaJedanZnak.contains(druga)) {
                        zaJedanZnak.add(druga);
                    }
                }
            }
        }

        zaSveZnakove.add(zaJedanZnak);
        prijelaziENKA.put(prva, zaSveZnakove);

        for (int i = 0; i < prijelaziENKA.get(prva).size(); i++) {
            ArrayList<String> prelaziU = prijelaziENKA.get(prva).get(i);
            if (!prelaziU.isEmpty()) {
                for (String s : prelaziU) {
                    String stavka3 = s.split("\\[")[0];
                    ArrayList<String> slijedi3 = new ArrayList<>(Arrays.asList(s.split("\\[")[1].split("]")[0].split(", ")));
                    if (!prijelaziENKA.containsKey(stavka3 + slijedi3)) {
                        dodajPrijelaze(stavka3, slijedi3);
                    }
                }
            }
        }

    }

    private static ArrayList<String> izracunajSlijedi(String stavka) {
        String cropped = stavka.substring(stavka.indexOf('*') + 1, stavka.indexOf('['));
        String[] split = cropped.trim().split(" ");
        Set<String> slijedi = new TreeSet<>();

        boolean prazan;
        int index = 1;

        do {
            prazan = false;

            if (index >= split.length) {
                break;
            }

            if (zavrsni.contains(split[index])) {
                slijedi.add(split[index]);
                break;
            }

            int rowIndex = nezavrsni.indexOf(split[index]);

            for (int i = nezavrsni.size(); i < nezavrsni.size() + zavrsni.size(); i++) {
                if (zapocinje[rowIndex][i]) {
                    slijedi.add(zavrsni.get(i - nezavrsni.size()));
                }
            }

            if (prazni.contains(split[index])) {
                prazan = true;
                index++;
            }
        } while (prazan && index < split.length);

        if (index >= split.length) {
            String toParse = stavka.substring(stavka.indexOf('[') + 1, stavka.indexOf(']'));
            slijedi.addAll(Arrays.asList(toParse.split(", ")));
        }

        return new ArrayList<>(slijedi);
    }

    private static void izracunajPrazne() {
        prazni = new LinkedHashSet<>();

        for (String key : produkcije.keySet()) {
            for (String produkcija : produkcije.get(key)) {
                if (produkcija.equals("$")) {
                    prazni.add(key);
                    break;
                }
            }
        }

        for (String nez : nezavrsni) {
            for (String produkcija : produkcije.get(nez)) {
                String[] steps = produkcija.split(" ");

                int index = 0;

                while (index < steps.length && prazni.contains(steps[index])) {
                    index++;
                }

                if (index == steps.length) {
                    prazni.add(nez);
                }
            }
        }
    }

    private static void izracunajZapocinjeIzravno() {
        zapocinjeIzravno = new boolean[nezavrsni.size()][nezavrsni.size() + zavrsni.size()];

        for (int i = 0; i < nezavrsni.size(); i++) {
            Arrays.fill(zapocinjeIzravno[i], false);     // CIJELA MATRICA CE BIT FALSE
        }

        for (String nez : nezavrsni) {
            for (String produkcija : produkcije.get(nez)) {
                if (!produkcija.equals("$")) {
                    String[] steps = produkcija.split(" ");

                    int index = 0;

                    while (index < steps.length && prazni.contains(steps[index])) {
                        index++;
                    }

                    if (index == steps.length) {
                        index--;
                    }

                    for (int i = 0; i <= index; i++) {
                        zapocinjeIzravno[nezavrsni.indexOf(nez)]
                                [zavrsni.contains(steps[i]) ?
                                zavrsni.indexOf(steps[i]) + nezavrsni.size() :
                                nezavrsni.indexOf(steps[i])] = true;

                    }
                }
            }
        }
    }

    private static void izracunajZapocinje() {
        zapocinje = zapocinjeIzravno;

        for (int i = 0; i < nezavrsni.size(); i++) {
            zapocinje[i][i] = true;                   // refleksivnost
        }

        // malo messy ali dolje slijedi tranzitivnost

        boolean change;
        do {
            change = false;
            for (int i = 0; i < nezavrsni.size(); i++) {
                for (int j = 0; j < nezavrsni.size(); j++) {
                    if (zapocinje[i][j]) {
                        for (int k = 0; k < nezavrsni.size() + zavrsni.size(); k++) {
                            if (zapocinje[j][k] && !zapocinje[i][k]) {
                                change = true;
                                zapocinje[i][k] = true;
                            }
                        }
                    }
                }
            }
        } while (change);
    }


    private static void generateDKA() {
        prijelaziDKA = new LinkedHashMap<>();
        dkaStavke = new LinkedHashMap<>();

        String trenutnaStavka = (String) prijelaziENKA.keySet().toArray()[0];

        Set<String> stavke = new TreeSet<>();
        stavke.add(trenutnaStavka);

        dodajDKAStanje(0, stavke);
    }

    private static int dodajDKAStanje(int stanje, Set<String> stavke) {
        ArrayList<String> stavke2 = DKADodajEpsilone(stavke);

        Integer[] prazno = new Integer[zavrsni.size() + nezavrsni.size()];
        Arrays.fill(prazno, -1);

        prijelaziDKA.put(stanje, new ArrayList<>(Arrays.asList(prazno)));

        dkaStavke.put(stanje, new ArrayList<>(stavke2));


        Set<String> spremniZaNovoStanje;

        int trenutnoStanje = stanje;
        ArrayList<String> trenutneStavke = dkaStavke.get(trenutnoStanje);

        for (int i = 0; i < zavrsni.size() + nezavrsni.size(); i++) {
            spremniZaNovoStanje = new TreeSet<>();
            for (String stavka : trenutneStavke) {
                spremniZaNovoStanje.addAll(prijelaziENKA.get(stavka).get(i));
            }
            if (!spremniZaNovoStanje.isEmpty()) {
                boolean exists = false;

                ArrayList<String> spremniji = DKADodajEpsilone(spremniZaNovoStanje);

                int novoStanje = -1; // dkaStavke.values().contains(spremniji) ? dkaStavke. : -1;

                for (int j = 0; j < dkaStavke.size(); j++) {
                    if (dkaStavke.get(j).equals(spremniji)) {
                        novoStanje = j;
                        break;
                    }
                }

                if (novoStanje == -1) {
                    // ej, nema ovog stanja jos, napravi ga
                    ArrayList<Integer> current = prijelaziDKA.get(trenutnoStanje);
                    current.remove(i);
                    current.add(i, stanje + 1);
                    stanje = dodajDKAStanje(stanje + 1, spremniZaNovoStanje);
                } else {
                    ArrayList<Integer> current = prijelaziDKA.get(trenutnoStanje);
                    current.remove(i);
                    current.add(i, novoStanje);
                }
            }
        }
        return stanje;
    }

    private static ArrayList<String> DKADodajEpsilone(Set<String> stavke) {

        Set<String> stCpy = new TreeSet<>(stavke);

        boolean hasMore;

        do {
            hasMore = false;
            for (String stavka : stavke) {
                if (stCpy.addAll(prijelaziENKA.get(stavka).get(prijelaziENKA.get(stavka).size() - 1))) {
                    hasMore = true;
                }
            }
            stavke.addAll(stCpy);
        } while (hasMore);

        return new ArrayList<>(stavke);
    }

    private static void generateAkcija() {
        akcija = new ArrayList<>();

        for (Integer stanjeDKA : dkaStavke.keySet()) {
            ArrayList<String> redakAkcije = new ArrayList<>();
            //napuni s odbaci (-)
            for (int i = 0; i <= zavrsni.size(); i++)
                redakAkcije.add("-");

            ArrayList<String> sveStavkeUStanju = dkaStavke.get(stanjeDKA);
            ArrayList<Integer> prijelazi = prijelaziDKA.get(stanjeDKA);

            //za sve zavrsne
            for (int i = 0; i < zavrsni.size(); i++) {

                //ako postoji * na kraju radi REDUCIRAJ

                for (String stavka : sveStavkeUStanju) {
                    String stavkaBezSkupa = stavka.split("\\[")[0].trim();
                    if (stavkaBezSkupa.endsWith("*")) {
                        ArrayList<String> skupSlijedi = new ArrayList<>(Arrays.asList(stavka.split("\\[")[1].split("]")[0].split(", ")));
                        String redukcija = stavkaBezSkupa.replace(" *", "");
                        if (redukcija.endsWith("->")) redukcija += " $";
                        int redRedukcije = produkcijeZaAckijuINovoStanje.indexOf(redukcija);

                        for (String znak : skupSlijedi) {
                            if (znak.equals(";")) {

                                int uTablici = (redakAkcije.get(zavrsni.size()).equals("-") || redakAkcije.get(zavrsni.size()).equals("+")) ? -1 : Integer.parseInt(redakAkcije.get(zavrsni.size()).substring(1));
                                if (uTablici == -1 || uTablici > redRedukcije) {
                                    redakAkcije.remove(zavrsni.size());
                                    if (redRedukcije != 0)
                                        redakAkcije.add(zavrsni.size(), "r" + redRedukcije);
                                    else
                                        redakAkcije.add(zavrsni.size(), "+");
                                }
                            } else {
                                int uTablici = redakAkcije.get(zavrsni.indexOf(znak)).equals("-") ? -1 : Integer.parseInt(redakAkcije.get(zavrsni.indexOf(znak)).substring(1));
                                if (uTablici == -1 || uTablici > redRedukcije) {
                                    redakAkcije.remove(zavrsni.indexOf(znak));
                                    redakAkcije.add(zavrsni.indexOf(znak), "r" + redRedukcije);
                                }
                            }
                        }
                    }
                }

                //ako ikoje prelazi u drugo stanje dodaj pBROJ_STANJA_DKA
                Integer novoStanje = prijelazi.get(i);

                if (novoStanje != -1) { //ako postoji prijelaz radi POMAKNI
                    redakAkcije.remove(i);
                    redakAkcije.add(i, "p" + novoStanje);
                }

            }

            akcija.add(redakAkcije);
        }

    }

    private static void generateNovoStanje() {
        novoStanje = new ArrayList<>();

        for (Integer stanjeDKA : dkaStavke.keySet()) {
            ArrayList<String> redakNovoStanje = new ArrayList<>();

            for (int i = 0; i < nezavrsni.size(); i++)
                redakNovoStanje.add("-");

            ArrayList<Integer> prijelazi = prijelaziDKA.get(stanjeDKA);

            //za sve nezavrsnie
            for (int i = 0; i < nezavrsni.size(); i++) {
                Integer novoStanje = prijelazi.get(i + zavrsni.size());
                if (novoStanje != -1) {
                    redakNovoStanje.remove(i);
                    redakNovoStanje.add(i, "s" + novoStanje);
                }
            }

            novoStanje.add(redakNovoStanje);
        }
    }

    public static void write() throws IOException {
        StringBuilder sb = new StringBuilder();

        Path localDir = Paths.get("").toAbsolutePath();
        File dir = new File(localDir + "/analizator");
        File file = new File(dir, "LibSA.txt");


        //Nezavrsni
        sb.append(String.join(", ", nezavrsni)).append("\n");

        //Zavrsni
        sb.append(String.join(", ", zavrsni)).append("\n");

        //Sinkronizacijski
        sb.append(String.join(", ", sinkronizacijski)).append("\n");

        sb.append("Produkcije:\n");
        produkcijeZaAckijuINovoStanje.forEach(p -> sb.append(p).append("\n"));

        sb.append("Akcija:\n");
        akcija.forEach(a -> sb.append(String.join(", ", a)).append("\n"));

        sb.append("NovoStanje:\n");
        novoStanje.forEach(a -> sb.append(String.join(", ", a)).append("\n"));

        file.delete();
        file.createNewFile();
        RandomAccessFile fw = new RandomAccessFile(file, "rw");
        fw.seek(0);
        fw.write(sb.toString().getBytes());
        fw.close();

    }

    public static void soutForTesting() {
        System.out.println("Nezavrsni: " + nezavrsni);
        System.out.println("Zavrsni: " + zavrsni);
        System.out.println("Sinkronizacijski: " + sinkronizacijski);
        System.out.println("Producije: " + produkcije);
        System.out.println();
        System.out.println("ENKA===========================");
        System.out.println("\"Stavka\"");
        System.out.println("-----------------------------------------");
        stavke.forEach(k -> System.out.println("\"" + k + "\" "));
        System.out.println();
        System.out.println("Prijelazi");
        System.out.println("-------------------------------");
        System.out.print("                                                  ");
        zavrsni.forEach(z -> System.out.printf("%50s", z));
        nezavrsni.forEach(n -> System.out.printf("%50s", n));
        System.out.printf("%50s", "$");
        System.out.println();
        prijelaziENKA.forEach((k, v) -> {
            System.out.printf("%50s", k);
            v.forEach(e -> System.out.printf("%50s", e));
            System.out.println();
        });

        System.out.println("\nPrazni:");
        System.out.println("----------------------");

        prazni.forEach(e -> System.out.print(e + " "));

        System.out.println("\n\nZapocinje:\n");
        System.out.println("----------------------");

        for (int i = 0; i < nezavrsni.size(); i++) {
            for (int j = 0; j < nezavrsni.size() + zavrsni.size(); j++) {
                System.out.print(zapocinje[i][j] + " ");
            }
            System.out.println();
        }


        System.out.println("\n DKA stavke:");
        dkaStavke.forEach((k, v) -> {
            System.out.print(k + " ");
            v.forEach(e -> System.out.print(e + "    "));
            System.out.println();
        });

        System.out.println("\n DKA prijelazi:");
        prijelaziDKA.forEach((k, v) -> {
            System.out.print(k + " ");
            v.forEach(e -> System.out.print(e + "    "));
            System.out.println();
        });

        System.out.println();
        System.out.println("Akcija");
        System.out.println("----------------------");
        System.out.print("          ");
        zavrsni.forEach(z -> System.out.printf("%10s", z));
        System.out.printf("%10s", ";");
        System.out.println();
        for (int i = 0; i < dkaStavke.size(); i++) {
            System.out.printf("%10d", i);
            akcija.get(i).forEach((s -> System.out.printf("%10s", s)));
            System.out.println();
        }

        System.out.println();
        System.out.println("NovoStanje");
        System.out.println("----------------------");
        System.out.print("          ");
        nezavrsni.forEach(z -> System.out.printf("%10s", z));
        System.out.println();
        for (int i = 0; i < dkaStavke.size(); i++) {
            System.out.printf("%10d", i);
            novoStanje.get(i).forEach((s -> System.out.printf("%10s", s)));
            System.out.println();
        }


    }


}
