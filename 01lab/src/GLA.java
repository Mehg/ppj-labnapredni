import java.util.LinkedHashMap;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class GLA {
    public static List<String> text = new ArrayList<>();
    public static int currentIndex;
    public static TreeMap<String, String> regularDefinitions = new TreeMap<>();
    public static ArrayList<String> states = new ArrayList<>();
    public static ArrayList<String> tokenTypes = new ArrayList<>();
    public static ArrayList<Rule> rules = new ArrayList<>();
    public static StringBuilder sb = new StringBuilder();


    public static class PairOfStates {
        private final int stateLeft;
        private final int stateRight;

        public PairOfStates(int stateLeft, int stateRight) {
            this.stateLeft = stateLeft;
            this.stateRight = stateRight;
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine())
            text.add(sc.nextLine());
        currentIndex = 0;

        addRegularDefinitions();
        addStates();
        addTokenTypes();
        addRules();

        resetFile();
    }

    public static void addRegularDefinitions() {
        while (!text.get(currentIndex).startsWith("%X")) {
            String line = text.get(currentIndex);
            StringBuilder key = new StringBuilder();
            String value;
            int i = 0;

            do {
                key.append(line.charAt(i++));
            } while (line.charAt(i) != '}');

            key.append("}");
            i += 2;
            value = line.substring(i);

            String finalValue = dereferenceRegularExpression(value);

            regularDefinitions.put(key.toString(), finalValue);
            currentIndex++;
        }
    }

    private static String dereferenceRegularExpression(String value) {
        String newValue;
        String finalValue;

        boolean replaced = true;

        while (replaced) {
            replaced = false;
            for (Map.Entry<String, String> definition : regularDefinitions.entrySet()) {
                if (value.contains(definition.getKey())) {
                    value = value.replace(definition.getKey(), "(" + definition.getValue() + ")");
                    replaced = true;
                }
            }
        }

//            DEPRECATED N ANNOYING
//
//            regularDefinitions.forEach((k, v) -> {
//                if (value.contains(k)) {
//                    newValue.set(value.replace(k, "(" + v + ")"));
//                }
//            });
//        finalValue = newValue.get();
//        return finalValue;

        return value;
    }

    public static void addStates() {
        if (text.get(currentIndex).startsWith("%X ")) {
            String line = text.get(currentIndex++).substring(3);
            String[] statesArray = line.split(" ");
            states.addAll(Arrays.asList(statesArray));
        }
    }

    public static void addTokenTypes() {
        if (text.get(currentIndex).startsWith("%L ")) {
            String line = text.get(currentIndex++).substring(3);
            String[] tokenTypesArray = line.split(" ");
            tokenTypes.addAll(Arrays.asList(tokenTypesArray));
        }
    }

    public static void addRules() {
        while (currentIndex != text.size()) {
            String line = text.get(currentIndex++).substring(1);
            String stanje = line.split(">")[0];
            String regularExpression = line.substring(line.indexOf(">") + 1);
            String tokenType = text.get(++currentIndex);

            for (String m : regularDefinitions.keySet()) {
                while (regularExpression.contains(m)) {
                    regularExpression = dereferenceRegularExpression(regularExpression);
                }
            }

            Automaton automaton = new Automaton(0);
            PairOfStates res = transform(regularExpression, automaton);
            automaton.setStartState(res.stateLeft);
            automaton.setAccept(res.stateRight);

            Rule rule = new Rule(stanje, regularExpression, automaton, tokenType);
            currentIndex++;

            while (currentIndex != text.size() && !text.get(currentIndex).startsWith("}")) {
                line = text.get(currentIndex++);
                if (line.split(" ").length > 1) {
                    String specialArgumentType = line.split(" ")[0];
                    SpecialArguments special;

                    if ("UDJI_U_STANJE".equals(specialArgumentType)) {
                        special = SpecialArguments.UDJI_U_STANJE;
                    } else if ("VRATI_SE".equals(specialArgumentType)) {
                        special = SpecialArguments.VRATI_SE;
                    } else {
                        throw new IllegalStateException("Unexpected value: " + specialArgumentType);
                    }
                    rule.add(special, line.split(" ")[1]);
                } else {
                    SpecialArguments special;
                    if (line.equals("NOVI_REDAK")) special = SpecialArguments.NOVI_REDAK;
                    else throw new IllegalArgumentException("Unexpected value: " + line);
                    rule.add(special, "");
                }
            }
            currentIndex++;
            rules.add(rule);
        }
    }


    public static int newState(Automaton automaton) {
        automaton.setNumberOfStates(automaton.getNumberOfStates() + 1);
        return automaton.getNumberOfStates() - 1;
    }

    public static boolean isOperator(String expression, int i) {
        int br = 0;
        while ((i - 1) >= 0 && expression.charAt(i - 1) == '\\') {
            br++;
            i--;
        }
        return br % 2 == 0;
    }

    public static PairOfStates transform(String expression, Automaton automaton) {
        ArrayList<String> choices = new ArrayList<>();
        int noBrackets = 0;
        int beginIndex = 0;
        int endIndex = 0;

        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(' && isOperator(expression, i))
                noBrackets++;
            else if (expression.charAt(i) == ')' && isOperator(expression, i))
                noBrackets--;
            else if (noBrackets == 0 && expression.charAt(i) == '|' && isOperator(expression, i)) {
                choices.add(expression.substring(beginIndex, i));
                beginIndex = ++i;
            }
            endIndex = i;
        }

        //SUS
        if (!choices.isEmpty() && endIndex != 0)
            choices.add(expression.substring(beginIndex, endIndex + 1));

        int leftState = newState(automaton);
        int rightState = newState(automaton);
        if (!choices.isEmpty()) {
            for (int i = 0; i < choices.size(); i++) {
                PairOfStates temp = transform(choices.get(i), automaton);
                automaton.addEpsilonTransition(new int[]{leftState, temp.stateLeft});
                automaton.addEpsilonTransition(new int[]{temp.stateRight, rightState});
            }
        } else {
            boolean prefix = false;
            int lastState = leftState;
            for (int i = 0; i < expression.length(); i++) {
                int a, b;
                if (prefix) {
                    prefix = false;
                    char transitioningChar;
                    if (expression.charAt(i) == 't')
                        transitioningChar = '\t';
                    else if (expression.charAt(i) == 'n')
                        transitioningChar = '\n';
                    else if (expression.charAt(i) == '_')
                        transitioningChar = ' ';
                    else
                        transitioningChar = expression.charAt(i);

                    a = newState(automaton);
                    b = newState(automaton);
                    //da ne spremamo kao epsilon
                    if (transitioningChar == '$') {
                        automaton.addTransition(new int[]{a, b}, String.valueOf("\\$"));
                    } else {
                        automaton.addTransition(new int[]{a, b}, String.valueOf(transitioningChar));
                    }
                } else {
                    if (expression.charAt(i) == '\\') {
                        prefix = true;
                        continue;
                    }
                    if (expression.charAt(i) != '(') {
                        a = newState(automaton);
                        b = newState(automaton);
                        if (expression.charAt(i) == '$') {
                            automaton.addEpsilonTransition(new int[]{a, b});
                        } else {
                            automaton.addTransition(new int[]{a, b}, String.valueOf(expression.charAt(i)));
                        }
                    } else {
                        int j;
                        int noBracc = 1;
                        boolean isFound = false;

                        for (j = i + 1; j < expression.length(); j++) {
                            if (expression.charAt(j) == '(' && isOperator(expression, j))
                                noBracc++;
                            else if (expression.charAt(j) == ')' && isOperator(expression, j))
                                noBracc--;
                            if (noBracc == 0 && isOperator(expression, j)) {
                                break;
                            }
                        }

                        PairOfStates temp = transform(expression.substring(i + 1, j), automaton);
                        a = temp.stateLeft;
                        b = temp.stateRight;
                        i = j;
                    }
                }
                if (i + 1 < expression.length() && expression.charAt(i + 1) == '*') {
                    int x = a;
                    int y = b;
                    a = newState(automaton);
                    b = newState(automaton);
                    automaton.addEpsilonTransition(new int[]{a, x});
                    automaton.addEpsilonTransition(new int[]{y, b});
                    automaton.addEpsilonTransition(new int[]{a, b});
                    automaton.addEpsilonTransition(new int[]{y, x});
                    i++;
                }
                automaton.addEpsilonTransition(new int[]{lastState, a});
                lastState = b;
            }
            automaton.addEpsilonTransition(new int[]{lastState, rightState});
        }
        return new PairOfStates(leftState, rightState);
    }

    private enum SpecialArguments {
        NOVI_REDAK, UDJI_U_STANJE, VRATI_SE
    }

    public static class Rule {
        public String state;
        public String regex;
        public Automaton automaton;
        public String tokenType;
        public Map<SpecialArguments, String> argumentActions;

        public Rule(String state, String regex, Automaton automaton, String tokenType, Map<SpecialArguments, String> argumentActions) {
            this.state = state;
            this.regex = regex;
            this.automaton = automaton;
            this.tokenType = tokenType;
            this.argumentActions = argumentActions;
        }

        public Rule(String state, String regex, Automaton automaton, String tokenType) {
            this.state = state;
            this.regex = regex;
            this.automaton = automaton;
            this.tokenType = tokenType;
            argumentActions = new TreeMap<>();
        }

        public Rule(String state, String regex) {
            this.state = state;
            this.regex = regex;
        }

        public Rule() {
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getRegex() {
            return regex;
        }

        public void setRegex(String regex) {
            this.regex = regex;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

        public Map<SpecialArguments, String> getArgumentActions() {
            return argumentActions;
        }

        public void setArgumentActions(Map<SpecialArguments, String> argumentActions) {
            this.argumentActions = argumentActions;
        }

        public void add(SpecialArguments k, String v) {
            argumentActions.put(k, v);
        }

        public Automaton getAutomaton() {
            return automaton;
        }
    }

    public static class Automaton {

        private int numberOfStates;
        private int startState;
        private int accept;
        private final LinkedHashMap<ArrayList<String>, ArrayList<Integer>> transitions;

        public Automaton(int numberOfStates, int startState, int accept, LinkedHashMap<ArrayList<String>, ArrayList<Integer>> transitions) {
            this.numberOfStates = numberOfStates;
            this.startState = startState;
            this.accept = accept;
            this.transitions = transitions;
        }

        public Automaton(int numberOfStates) {
            this.numberOfStates = numberOfStates;
            transitions = new LinkedHashMap<>();
        }

        public void addEpsilonTransition(int[] pairOfStates) {

            ArrayList<String> key = new ArrayList<>();
            key.add(String.valueOf(pairOfStates[0]));
            key.add("$");
            ArrayList<Integer> value;
            if (transitions.containsKey(key)) {
                value = transitions.get(key);
            } else {
                value = new ArrayList<>();
            }
            value.add(pairOfStates[1]);
            transitions.put(key, value);
        }

        public void addTransition(int[] pair, String transitionChar) {
            ArrayList<String> key = new ArrayList<>();
            key.add(String.valueOf(pair[0]));
            key.add(transitionChar);
            ArrayList<Integer> value;
            if (transitions.containsKey(key)) {
                value = transitions.get(key);
            } else {
                value = new ArrayList<>();
            }
            value.add(pair[1]);
            transitions.put(key, value);
        }

        public void setStartState(int startState) {
            this.startState = startState;
        }

        public void setAccept(int accept) {
            this.accept = accept;
        }

        public int getAccept() {
            return accept;
        }

        public int getStartState() {
            return startState;
        }

        public int getNumberOfStates() {
            return numberOfStates;
        }

        public void setNumberOfStates(int numberOfStates) {
            this.numberOfStates = numberOfStates;
        }

        public LinkedHashMap<ArrayList<String>, ArrayList<Integer>> getTransitions() {
            return transitions;
        }
    }

    public static void resetFile() throws IOException {
        sb.append(
                "\n" +
                        "import java.util.ArrayList;\n" +
                        "import java.util.LinkedHashMap;\n" +
                        "import java.util.TreeMap;\n" +
                        "\n" +
                        "public class LibLA {\n" +
                        "\tpublic static ArrayList<String> states = new ArrayList<>();\n" +
                        "\tpublic static ArrayList<String> tokenTypes = new ArrayList<>();\n" +
                        "\tpublic static ArrayList<LA.Rule> rules = new ArrayList<>();\n" );


        sb.append("    public static ArrayList<String> getStates() {\n");

        for (String state : states)
            sb.append("        states.add(\"").append(state).append("\");\n");
        sb.append("        return states;\n" +
                "    }\n");

        sb.append("    public static ArrayList<String> getTokenTypes() {\n");
        for (String token : tokenTypes)
            sb.append("        tokenTypes.add(\"").append(token).append("\");\n");
        sb.append("        return tokenTypes;\n" +
                "    }\n");

        sb.append("    public static ArrayList<LA.Rule> getRules() {\n");

        sb.append("rules = new ArrayList<LA.Rule>() {{\n");
        for (Rule rule : rules) {
            sb.append("\t\tadd( new LA.Rule(");
            sb.append("\"").append(rule.getState().replace("\\", "\\\\")
                    .replace("\n", "\\n")
                    .replace("\t", "\\t").replace("\"", "\\\"")).append("\"");
            sb.append(", ");
            sb.append("\"").append(rule.getRegex().replace("\\", "\\\\")
                    .replace("\n", "\\n")
                    .replace("\t", "\\t").replace("\"", "\\\"")).append("\"");
            sb.append(", new LA.Automaton(");
            sb.append(rule.getAutomaton().getNumberOfStates());
            sb.append(", ");
            sb.append(rule.getAutomaton().getStartState());
            sb.append(", ");
            sb.append(rule.getAutomaton().getAccept());
            sb.append(", new LinkedHashMap<ArrayList<String>, ArrayList<Integer>>() {{");
            for (Map.Entry<ArrayList<String>, ArrayList<Integer>> entry : rule.getAutomaton().getTransitions().entrySet()) {
                sb.append("\t\tput(new ArrayList<String>(){{ add(\"");
                sb.append(entry.getKey().get(0));
                sb.append("\"); add(\"");
                sb.append(entry.getKey().get(1).replace("\\", "\\\\")
                        .replace("\n", "\\n")
                        .replace("\t", "\\t").replace("\"", "\\\""));
                sb.append("\");}}, new ArrayList<Integer>(){{");
                for (Integer integer : entry.getValue()) {
                    sb.append(" add(");
                    sb.append(integer);
                    sb.append(");");
                }
                sb.append("}}");
                sb.append(");\n");
            }
            sb.append("}})");
            sb.append(", ");
            sb.append("\"").append(rule.getTokenType()).append("\"");
            sb.append(", new TreeMap<LA.SpecialArguments, String>() {{");
            for (Map.Entry<SpecialArguments, String> entry : rule.getArgumentActions().entrySet()) {
                sb.append("\t\tput(");
                sb.append("LA.SpecialArguments.");
                sb.append(entry.getKey());
                sb.append(", ");
                sb.append("\"").append(entry.getValue()
                        .replace("\\", "\\\\")
                        .replace("\n", "\\n")
                        .replace("\t", "\\t").replace("\"", "\\\"")).append("\"");
                sb.append(");\n");
            }
            sb.append("}}));\n");
        }
        sb.append("}};");
        sb.append("        return rules;\n" +
                "    }\n").append("}\n");


        Path localDir = Paths.get("").toAbsolutePath();
        File dir = new File(localDir + "/analizator");
        File file = new File(dir, "LibLA.java");

        boolean a = file.delete();
        file.createNewFile();
        RandomAccessFile fw = new RandomAccessFile(file, "rw");
        fw.seek(0);
        fw.write(sb.toString().getBytes());
        fw.close();
    }

}
