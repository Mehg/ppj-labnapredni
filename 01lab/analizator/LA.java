import javax.sound.midi.Soundbank;
import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class LA {
    public static String text = "";
    public static int currentStartIndex;
    public static int currentEndIndex;
    private static String currentText;
    public static String currentState;
    public static int currentLine;
    private static ArrayList<String> states;
    private static ArrayList<String> tokenTypes;
    private static ArrayList<Rule> rules;
    public static StringBuilder sb;
    public static Stack<String> stack = new Stack<>();

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine())
            text += sc.nextLine() + "\n";
        text = text.replace("$", "\\$");

        states = LibLA.getStates();
        tokenTypes = LibLA.getTokenTypes();
        rules = LibLA.getRules();

        currentState = states.get(0);
        currentLine = 1;
        currentEndIndex = text.length();
        currentStartIndex = 0;
        currentText = text;


        sb = new StringBuilder();
        stack.push(currentState);

        while (currentStartIndex != currentText.length()) {
            int biggestYet = 0;
            Rule currentRule = null;
            for (Rule rule : rules) {
                if (rule.state.equals(currentState)) {
                    int temp = testAutomaton(currentText, rule.automaton);
                        if (temp > biggestYet) {
                        currentEndIndex = temp;
                        currentRule = rule;
                        biggestYet = temp;
                    }
                }
            }
            if (currentRule != null) {
                boolean newRow = false;
                for (SpecialArguments s : currentRule.getArgumentActions().keySet()) {
                    switch (s) {
                        case NOVI_REDAK: {
                            newRow = true;
                            break;
                        }
                        case UDJI_U_STANJE: {
                            currentState = currentRule.getArgumentActions().get(s);
                            stack.push(currentState);
                            break;
                        }
                        case VRATI_SE: {
                            currentEndIndex = Integer.parseInt(currentRule.getArgumentActions().get(s));
                            break;
                        }
                    }
                }
                if (!currentRule.getTokenType().equals("-")) {
                    sb.append(currentRule.getTokenType()).append(" ")
                            .append(currentLine).append(" ")
                            .append(currentText, currentStartIndex, currentEndIndex).append("\n");

                }
                currentStartIndex = currentEndIndex;
                currentEndIndex = currentText.length();

                currentText = currentText.substring(currentStartIndex, currentEndIndex);
                currentStartIndex = 0;
                currentEndIndex = currentText.length();
                if (newRow) currentLine++;

            } else { //TODO oporavak - vracanje stanja i svasta nesta
                System.err.println(currentLine + " " + currentText.charAt(currentStartIndex)+" can't identify");
                currentText = currentText.substring(currentStartIndex+1);
                currentStartIndex=0;
            }

        }

        System.out.println(sb.toString());
    }

    //TODO possibly delet this
    //currently: za svaki char (neovisno o proslome - BAD) testaj
    //ako value.equals char napravi prijelaz
    //but epsiloni! ne radis ih
    //napravi ovo dobro
    //public static int testAutomaton(String test, Automaton automaton) {
//        Set<Integer> currentStates = new HashSet<>();
//        Set<Integer> currentStatesCopy = new HashSet<>();//ZA ITERIRANJE
//        Set<Integer> nextStates = new HashSet<>();
//        currentStates.add(automaton.getStartState());
//        currentStatesCopy.addAll(currentStates);
//        int lastIndex = -1;
//        char[] chars = test.toCharArray();
//        boolean epsilon = true;
//
//        //EPSILON OKOLINA
//        if (automaton.getTransitions().containsValue("$")) {
//            while (epsilon) {
//                epsilon = false;
//                for (Map.Entry<String[], Integer> entry : automaton.getTransitions().entrySet()) {
//                    for (Integer state : currentStatesCopy) {
//                        if (entry.getKey()[0] == state && entry.getValue().equals("$") && !currentStates.contains(entry.getKey()[1])) {
//                            currentStates.add(entry.getKey()[1]);
//                            epsilon = true;
//                        }
//
//                        if (automaton.getAccept() == state) {
//                            lastIndex = 0;
//                        }
//                    }
//                }
//                currentStatesCopy.clear();
//                currentStatesCopy.addAll(currentStates);
//            }
//        }
//
//        for (int i = 0; i < chars.length; i++) {
//            epsilon = true;
//
//            //JEDAN NE-EPSILON PRIJELAZ
//            for (Map.Entry<int[], String> entry : automaton.getTransitions().entrySet()) {
//                for (Integer state : currentStates) {
//                    if (state == entry.getKey()[0]
//                            && Character.toString(chars[i]).equals(entry.getValue())) {
//                        nextStates.add(entry.getKey()[1]);
//                    }
//                }
//            }
//
//            currentStates.clear();
//            currentStates.addAll(nextStates);
//            nextStates.clear();
//            currentStatesCopy.clear();
//            currentStatesCopy.addAll(currentStates);
//
//            if (currentStates.isEmpty()) {
//                return lastIndex;
//            }
//
//            //SVI EPSILON PRIJELAZI KOJI MOGU
//            if (automaton.getTransitions().containsValue("$")) {
//                while (epsilon) {
//                    epsilon = false;
//                    for (Map.Entry<int[], String> entry : automaton.getTransitions().entrySet()) {
//                        for (Integer state : currentStatesCopy) {
//                            if (entry.getKey()[0] == state && entry.getValue().equals("$") && !currentStates.contains(entry.getKey()[1])) {
//                                currentStates.add(entry.getKey()[1]);
//                                epsilon = true;
//                            }
//
//                            if (automaton.getAccept() == state) {
//                                lastIndex = i;
//                            }
//                        }
//                    }
//                    currentStatesCopy.clear();
//                    currentStatesCopy.addAll(currentStates);
//                }
//            }
//        }
//        return lastIndex;
    //}

    //TODO
    public static int testAutomaton(String test, Automaton automaton) {
        Set<Integer> currentStates = new HashSet<>();
        currentStates.add(automaton.getStartState());
        Set<Integer> nextStates = new HashSet<>(currentStates);
        int lastIndex = -1;
        char[] chars = test.toCharArray();
        boolean epsilon = true;
        boolean found = false;

        //EPSILON OKOLINA
        while (epsilon) {
            epsilon = false;
            for (Integer state : currentStates) {
                List<String> testList = new ArrayList <> ();
                testList.add(Integer.toString(state));
                testList.add("$");
                if (automaton.getTransitions().containsKey(testList)) {
                    if (nextStates.addAll(automaton.getTransitions().get(testList))) {
                        epsilon = true;
                    }
                }
            }
            currentStates.addAll(nextStates);
        }

        //AYOO, ACCEPTABLE CHEECK
        for (Integer state : currentStates) {
            if (state.equals(automaton.getAccept())) {
                lastIndex = 0;
                break;
            }
        }


        int i = 0;                                      //indeks u ulaznom nizu

        //SAMO JEDAN NE EPSILON PRIJELAZ, OK? DA NAM NE PROMAKNU VAZNI EPSILON PRIJELAZI.


        //DEPRECATED DEPRECATED DEPRECATED

//        while (i < chars.length) {                                                                  //dok god ima charova u ulaznom nizu
//            found = false;                                                                          //resetiraj found zastavicu
//            nextStates = new HashSet<>();                                                           //make dat shit clean again
//            for (Map.Entry<String[], Integer> transition : automaton.getTransitions().entrySet()) { //za sve tranzicije
//                for (Integer state : currentStates) {                                               //za sva trenutna stanja
//                    if (state.equals(Integer.valueOf(transition.getKey()[0]))                       //ako si u stanju u tranziciji
//                            && Character.toString(chars[i]).equals(transition.getKey()[1])) {       //i trenutni input char je kao i u tranziciji
//                        nextStates.add(transition.getValue());                                      //dodaj zavrsni value tranzicije u nova stanja
//                        i++;
//                        found = true;
//                    }
//
//                    if (found) {
//                        break;
//                    }
//                }
//                if (found) {
//                    break;
//                }
//            }

        while (i < chars.length) {

            String input = Character.toString(chars[i]);
            if (input.equals("\\")&&Character.toString(chars[i+1]).equals("$")) {
                input = input.concat(Character.toString(chars[++i]));
            }

            nextStates = new HashSet<>();
            boolean consumed = false;
            for (Integer state : currentStates) {
                List<String> testList = new ArrayList <> ();
                testList.add(Integer.toString(state));
                testList.add(input);
                if (automaton.getTransitions().containsKey(testList)) {
                    nextStates.addAll(automaton.getTransitions().get(testList));
                    //i++;
                    consumed = true;
                }
            }

            if (consumed) {
                i++;
            }
                                                            //yeet bitch, make sure u empty
            currentStates = new HashSet<>(nextStates);      //and fill yourshelf wifh the good shtuf

            if (currentStates.isEmpty()) {
                return lastIndex;
            }

            epsilon = true;
            while (epsilon) {
                epsilon = false;
                for (Integer state : currentStates) {
                    List<String> testList = new ArrayList <> ();
                    testList.add(Integer.toString(state));
                    testList.add("$");
                    if (automaton.getTransitions().containsKey(testList)) {
                        if (nextStates.addAll(automaton.getTransitions().get(testList))) {
                            epsilon = true;
                        }
                    }
                }
                currentStates.addAll(nextStates);
            }

//            //EPSILON CHECCCC
//
//            DEPRECATED LMAO
//
//            epsilon = true;
//            while (epsilon) {
//                epsilon = false;
//                for (Map.Entry<String[], Integer> transition : automaton.getTransitions().entrySet()) { //za sve tranzicije
//                    for (Integer state : currentStates) {                                               //za sva trenutna stanja
//                        if (state.equals(Integer.valueOf(transition.getKey()[0]))                       //ako si u pocetnom stanju tranzicije
//                                && transition.getKey()[1].equals("$")) {                                //i char tranzicije je epsilon
//                            if (nextStates.add(transition.getValue())) {                                //ako currentStates nije sadrzavao
//                                epsilon = true;                                                         //to stanje, zabiljezi to
//                            }
//                        }
//                    }
//                }
//            }


            //AYOOO;;; ACCEPPTPBAELLEE CHECKKq
            for (Integer state : currentStates) {
                if (state.equals(automaton.getAccept())) {
                    lastIndex = i;
                    break;
                }
            }
        }
        return lastIndex;
    }



    public enum SpecialArguments {
        NOVI_REDAK, UDJI_U_STANJE, VRATI_SE
    }

    public static class Rule {
        public String state;
        public String regex;
        public Automaton automaton;
        public String tokenType;
        public Map<SpecialArguments, String> argumentActions;

        public Rule(String state, String regex, Automaton automaton, String tokenType, Map<SpecialArguments, String> argumentActions) {
            this.state = state;
            this.regex = regex;
            this.automaton = automaton;
            this.tokenType = tokenType;
            this.argumentActions = argumentActions;
        }

        public Rule(String state, String regex, Automaton automaton, String tokenType) {
            this.state = state;
            this.regex = regex;
            this.automaton = automaton;
            this.tokenType = tokenType;
            argumentActions = new TreeMap<>();
        }

        public Rule(String state, String regex) {
            this.state = state;
            this.regex = regex;
        }

        public Rule() {
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getRegex() {
            return regex;
        }

        public void setRegex(String regex) {
            this.regex = regex;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

        public Map<SpecialArguments, String> getArgumentActions() {
            return argumentActions;
        }

        public void setArgumentActions(Map<SpecialArguments, String> argumentActions) {
            this.argumentActions = argumentActions;
        }

        public void add(SpecialArguments k, String v) {
            argumentActions.put(k, v);
        }

        public Automaton getAutomaton() {
            return automaton;
        }
    }

    public static class Automaton {

        private int numberOfStates;
        private int startState;
        private int accept;
        private final LinkedHashMap<ArrayList<String>, ArrayList<Integer>> transitions;

        public Automaton(int numberOfStates, int startState, int accept, LinkedHashMap<ArrayList<String>, ArrayList<Integer>> transitions) {
            this.numberOfStates = numberOfStates;
            this.startState = startState;
            this.accept = accept;
            this.transitions = transitions;
        }

        public Automaton(int numberOfStates) {
            this.numberOfStates = numberOfStates;
            transitions = new LinkedHashMap<>();
        }

        public void addEpsilonTransition(int[] pairOfStates) {
            ArrayList<String> key = new ArrayList<>();
            key.add(String.valueOf(pairOfStates[0]));
            key.add("$");
            ArrayList<Integer> value;
            if (transitions.containsKey(key)) {
                value = transitions.get(key);
            } else {
                value = new ArrayList<>();
            }
            value.add(pairOfStates[1]);
            transitions.put(key, value);
        }

        public void addTransition(int[] pair, String transitionChar) {
            ArrayList<String> key = new ArrayList<>();
            key.add(String.valueOf(pair[0]));
            key.add(transitionChar);
            ArrayList<Integer> value;
            if (transitions.containsKey(key)) {
                value = transitions.get(key);
            } else {
                value = new ArrayList<>();
            }
            value.add(pair[1]);
            transitions.put(key, value);
        }

        public void setStartState(int startState) {
            this.startState = startState;
        }

        public void setAccept(int accept) {
            this.accept = accept;
        }

        public int getAccept() {
            return accept;
        }

        public int getStartState() {
            return startState;
        }

        public int getNumberOfStates() {
            return numberOfStates;
        }

        public void setNumberOfStates(int numberOfStates) {
            this.numberOfStates = numberOfStates;
        }

        public LinkedHashMap<ArrayList<String>, ArrayList<Integer>> getTransitions() {
            return transitions;
        }
    }


}
